#ifndef DELAUNAY_HPP
#define DELAUNAY_HPP

#include <Godot.hpp>
#include <Geometry.hpp>

namespace godot {

	class Delaunay : public Object {
		GODOT_CLASS(Delaunay, Geometry);

		public:
			static void _register_methods();
			Delaunay();
			~Delaunay();
			static void triangulate(int points);
	};
}

#endif /* !DELAUNAY_HPP */
