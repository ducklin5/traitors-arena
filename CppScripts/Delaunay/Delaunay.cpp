#include "Delaunay.hpp"

using namespace godot;

Delaunay::Delaunay() {
}

Delaunay::~Delaunay() {
    // add your cleanup here
}

void Delaunay::triangulate(int points) {}


void Delaunay::_register_methods() {
    register_method("triangulate", &Delaunay::triangulate);
}