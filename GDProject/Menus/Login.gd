extends Panel

var MainMenu = load("res://Menus/Main.tscn")
onready var iList = $"vbox/ItemList"
var profiles = []

func _ready(): refreshList()

func refreshList():
	profiles = []
	iList.clear()
	for id in Profile.getIdList():
		var profile = Profile.fromId(id)
		profiles.append(profile)
		iList.add_item(profile.name)

func _on_Create_pressed(): $"Create".show()

func _on_btnCreate_pressed():
	var profile = Profile.new()
	profile.name = $"Create/LineEdit".text
	profile.save()
	$"Create".hide()
	refreshList()

func _on_btnCancel_pressed(): $"Create".hide()

func getSelectedProfile():
	var selected  = Array(iList.get_selected_items())
	if selected.empty(): return null
	return profiles[selected[0]]

func _on_Delete_pressed():
	var profile = getSelectedProfile()
	if profile:
		Profile.delete(profile.id)
		refreshList()

func _on_Select_pressed():
	var profile = getSelectedProfile()
	if profile:
		User.profile = profile
		Globals.setScene(MainMenu.instance())