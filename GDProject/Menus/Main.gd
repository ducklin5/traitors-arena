extends PanelContainer

var profile

func _ready():
	var nodes = getAllChildren(self)
	for node in nodes:
		if node is Button:
			node.connect("button_down", self, "playBtnSound")

func getAllChildren(node):
	var children = []
	for child in node.get_children():
		children.append(child)
		if child.get_child_count() > 0:
			children += getAllChildren(child)
	return children

func playBtnSound():
	$AudioPlayer.play()
