extends PanelContainer

func _ready():
	var args = Array(OS.get_cmdline_args())
	if args.has("-host"):
		$"vbox/Top/HostLabel".show()
		$"vbox/Panels/Lobby".show()
		$"vbox/Panels/Profile".hide()
		OS.window_resizable = false
		OS.window_size = Vector2(600,700)
		host(args)
	else:
		$"vbox/Top/HostLabel".hide()
		$"vbox/Panels/Lobby".hide()
		$"vbox/Panels/Profile".show()

func host(args):
	var config = {"maxPlayers": 2.0,
	"name": "Custom Server",
	"players": 0, "seed": 959379415,
	"spectators": 4.0,"teams": 2.0,
	"vesselsPerTeam": 1.0}
		
	for arg in args:
		if arg.begins_with("-config="):
			arg = arg.right(8).replace('\'', '"')
			config = str2var(arg)
	
	if config is Dictionary:
		$vbox/Panels/Lobby.setMode(1)
		Network.startHosting(config)
	else: 
		print("No config provided")
		get_tree().quit()
