extends Panel
onready var NameEdit = $hbox/vbox/NameEdit
onready var TeamsSpin = $hbox/vbox/TeamsSpin
onready var PlayersSpin = $hbox/vbox/PlayersSpin
onready var SpecSpin = $hbox/vbox/SpectatorsSpin
onready var SeedEdit =  $hbox/vbox2/SeedEdit
onready var BtnHost = $hbox/vbox2/BtnHost
onready var MapNode = $hbox/vbox2/MapContainer/Map

signal mapPortComplete()

func getSeed():
	var seedString = (SeedEdit.text if !SeedEdit.text.empty() 
		else SeedEdit.placeholder_text)
	return seedString.hash()

func _ready():
	var pool = (util.cRange('0','9') 
				+ util.cRange('a','z') 
				+ util.cRange('A','Z'))
	randomize()
	SeedEdit.placeholder_text = util.randString(16, pool)
	MapNode._seed = getSeed()

func _on_BtnHost_pressed():
	for input in [NameEdit, TeamsSpin, PlayersSpin, SpecSpin, SeedEdit]:
		input.editable = false
	BtnHost.text = "Setting Up..."
	BtnHost.disabled = true
	btnHost_runHost()
	btnHost_connect()

func btnHost_runHost():
	# get variables
	var config = {
		"name": NameEdit.text,
		"teams": TeamsSpin.value,
		"vesselsPerTeam": PlayersSpin.value,
		"spectators": SpecSpin.value,
		"seed": getSeed(),
		"maxPlayers": PlayersSpin.value * TeamsSpin.value,
		"players": 0
	}
	var configString  = var2str(config).replace('\n','').replace('"', '\'')
	var args = ['-host', '-config='+configString]
	OS.execute(OS.get_executable_path(),PoolStringArray(args), false, [])

func btnHost_connect():
	var enet = NetworkedMultiplayerENet.new()
	enet.create_client("", Globals.PORTS[0])
	get_tree().set_network_peer(enet)
	print("My details: ", get_tree().get_network_unique_id())

func _on_SeedEdit_text_changed(new_text):
	$seedEditTimer.start()

func _on_seedEditTimer_timeout():
	var _seed = getSeed()
	if MapNode._seed != _seed:
		var thread = Thread.new()
		thread.start(MapNode, "setSeed", _seed)
		yield(MapNode,"mapRebuilt")
		thread.wait_to_finish()