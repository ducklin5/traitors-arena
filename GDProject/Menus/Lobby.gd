extends Panel

onready var PlayerList = $hbox/vbox/hbox/PlayerList
onready var StartBtn = $hbox/vbox/hostBox/Start

func _ready():
	Network.connect("playerRegistered", self, "playersChanged")
	Network.connect("gameStarted",self,"_on_gameStarted")

func playersChanged(players:Dictionary):
	PlayerList.clear()
	if is_network_master():
		StartBtn.disabled = players.size() < 1 or players.size() >= Network.gameConfig['maxPlayers']
		
	for id in players:
		var username = players[id]["name"]
		PlayerList.add_item(username)

func setMode(mode):
	match mode:
		0:
			$hbox/vbox/hostBox.hide()
			$hbox/vbox/joinBox.show()
			$hbox/hostInfo/Leave.text = "Leave"
		1:
			$hbox/vbox/hostBox.show()
			$hbox/vbox/joinBox.hide()
			$hbox/hostInfo/Leave.text = "Cancel"

func _on_Start_button_down():
	StartBtn.text = "Starting..."
	StartBtn.disabled = true
	yield(get_tree(), "idle_frame")
	yield(get_tree(), "idle_frame")
	Network.rpc("configureGame", Network.gameConfig)

func _on_gameStarted():
	StartBtn.text = "Started"
