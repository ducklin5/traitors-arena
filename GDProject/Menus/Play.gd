extends PanelContainer

func _ready():
	get_tree().connect("connected_to_server", self, "showLobby")

func showLobby():
	$VBar/Lobby.setMode(0)
	var idx = $VBar/Lobby.get_index()
	$VBar.setCurrent(idx)
	
	for i in $VBar.buttons:
		if i != idx:
			$VBar.buttons[i].disabled = true