extends Panel

var titles = ["Official", "Private","Name", "Address",
				"Players", "Max Players", "Started"]
var hostItems = {}
var hostData = {}

onready var Table = $hbox/vbox/HostList/Table
onready var HostInfo = $hbox/vbox2/HostInfo
onready var Search = $hbox/vbox/FilterBar/Search

func _ready():
	setUpTable()
	Search.get_node("PortSpin").min_value = Globals.PORTS[0]
	Search.get_node("PortSpin").max_value = Globals.PORTS[-1]
	_on_BtnRefresh_button_down()

func setUpTable():
	Table.set_column_titles_visible(true)
	for i in titles.size():
		Table.set_column_title(i,titles[i])
	Table.create_item()
	Table.hide_root = true
	Table.connect("item_selected", self, "onHostSelected")

func _on_BtnRefresh_button_down():
	var list = yield(DBServer.getHosts(), "completed")
	for id in list:
		if !hostItems.has(id):
			addHost(id, list[id])
		else:
			updateHost(id, list[id])
	for id in hostItems:
		if !list.has(id):
			removeHost(id)

func addHost(id, data):
	var item = Table.create_item()
	hostItems[id] = item
	updateHost(id,data)

func updateHost(id, data):
	var item = hostItems[id]
	for i in titles.size():
		match titles[i]:
			"Name": item.set_text(i, data.name)
			"Address": item.set_text(i, data.address)
			"Players": item.set_text(i, String(data.players))
			"Max Players": item.set_text(i, String(data.maxPlayers))
	hostData[item] = data

func removeHost(id):
	var item: TreeItem = hostItems[id]
	hostData.erase(item)
	Table.get_root().remove_child(item)
	hostItems.erase(id)

func onHostSelected():
	var item = Table.get_selected()
	var data = hostData[item]
	HostInfo.get_node("Name/Value").text = data.name
	Search.get_node("NameEdit").text = data.name
	Search.get_node("AddressEdit").text = data.address
	Search.get_node("PortSpin").value = data.port

func _on_Join_button_down():
	print("Joining network")
	
	var address = Search.get_node("AddressEdit").text
	var port = Search.get_node("PortSpin").value
	
	# set up the client
	var enet = NetworkedMultiplayerENet.new()
	enet.create_client(address, port)
	get_tree().set_network_peer(enet)
	print("My details: ", get_tree().get_network_unique_id())
