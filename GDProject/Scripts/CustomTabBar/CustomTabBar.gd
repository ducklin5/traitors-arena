extends BoxContainer

export var current = 1
export var barSizeRatio = 0.1 setget setBarSize

enum Style {Fit = 0, Scroll = 1}
export(Style) var BarStyle = Style.Scroll
export(PackedScene) var ButtonScene

var buttons = {}

func setCurrent(tabId):
	get_child(current).hide()
	get_child(tabId).show()
	current = tabId

func setBarSize( ratio ):
	barSizeRatio = ratio
	$Bar.size_flags_vertical = SIZE_EXPAND_FILL
	$Bar.size_flags_horizontal = SIZE_EXPAND_FILL
	$Bar/Tabs.size_flags_vertical = SIZE_EXPAND_FILL
	$Bar/Tabs.size_flags_horizontal = SIZE_EXPAND_FILL
	$Bar.size_flags_stretch_ratio = barSizeRatio

func _ready():
	$Bar.show()
	
	setBarSize(barSizeRatio)

	for idx in range(1, get_child_count()):
		var control :Control = get_child(idx)
		addTab(control)

func addTab(control: Control):
	if ! (control in get_children()):
		add_child(control)
	control.size_flags_horizontal = SIZE_EXPAND_FILL
	control.size_flags_vertical = SIZE_EXPAND_FILL
	
	var idx = control.get_index()
	if idx == current: control.show()
	else: control.hide()
	
	var tabConfig = []
	
	if tabConfig in control:
		tabConfig = control.tagConfig
	
	var button: Button = ButtonScene.instance() if ButtonScene else Button.new()
	button.name = control.name
	button.text = control.name
	
	buttons[idx] = button
	
	if BarStyle == Style.Fit:
		button.size_flags_horizontal = SIZE_EXPAND_FILL
		button.size_flags_vertical = SIZE_EXPAND_FILL
	
	if control.is_in_group("hideTab"):
		button.hide()
	
	button.connect("button_down", self, "setCurrent", [idx])
	$Bar/Tabs.add_child(button)