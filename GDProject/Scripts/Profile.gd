extends Object
class_name Profile

const path = "user://"

var id
var name
var picture
var rank

static func url(_id):
	return path + "profile.%s.json" % _id

static func fromId(_id):
	var data = DataParser.loadData(url(_id))
	return dict2inst(data)

static func getIdList():
	var ids = []
	var dir = Directory.new()
	dir.open(path)
	dir.list_dir_begin()
	while true:
		var file = dir.get_next()
		if file == "": break
		if file.match("profile.*.json"):
			ids.append(int(file.split('.')[1]))
	return ids

static func delete(_id):
	var dir = Directory.new()
	dir.remove(url(_id))

func _init():
	var ids = getIdList()
	while !id or id in ids:
		id = randi()

func getData():
	return inst2dict(self)

func save():
	DataParser.writeData(url(id), getData())
