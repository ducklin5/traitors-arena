extends Node

var  databaseUrl = "res://Database/Database_Items.json"
var data = null
var modifiedTime = 0
var file = File.new()

func getItemData(id :int):
	var newModTime = file.get_modified_time(databaseUrl)
	if data == null or newModTime > modifiedTime:
		data = DataParser.loadData(databaseUrl)
		modifiedTime = newModTime
	if !data.has(String(id)):
		print("Item doesnt exist in item database")
		return
	
	data[String(id)]["id"] = int(id)
	return data[String(id)]