extends Node

var thread = Thread.new()
var keepAlive = true
var aliveTimer = Timer.new()
var aliveDelay = 8

var gameScenePath = "res://Game/Game.tscn"
var game
var gameConfig = {}
var players = {}
var ready_peers = []

signal upnpComplete()
signal playerRegistered(players)
signal gameStarted()

func _ready():
	add_child(aliveTimer)
	aliveTimer.start(aliveDelay)
	
	get_tree().connect("connected_to_server", self, "hostConnected")
	get_tree().connect("network_peer_disconnected", self, "clientDisconnected")
	get_tree().connect("server_disconnected", self, "hostDisconnected")

# ===== START HOSTING ========================
func startHosting(config):
	var maxClients = config["teams"] * config["vesselsPerTeam"] + config["spectators"]
	var enetPort = createHost(Globals.PORTS, maxClients)
	if !enetPort:
		print("Error Creating host")
		return
	config["port"] = enetPort[1]
	
	# add a port mapping through upnp
	thread.start(self, "upnpMap", enetPort[1])
	yield(self, "upnpComplete")
	
	#finalize 
	gameConfig = config
	get_tree().set_network_peer(enetPort[0])
	var id = yield(DBServer.updateHost(gameConfig), "completed")
	keepAlive = true
	keepHostAlive(id)

func createHost(ports, maxClients):
	var enet = NetworkedMultiplayerENet.new()
	for port in ports:
		var result = enet.create_server(port, maxClients)
		if result == OK: return [enet, port]
	return null

func upnpMap(port)->void:
	var upnp = UPNP.new()
	print("started port mapping")
	upnp.discover(1000)
	if upnp.get_device_count() > 0:
		if upnp.get_gateway() and upnp.get_gateway().is_valid_gateway():
			upnp.add_port_mapping(port, port, 'TraitorsArena', 'UDP')
			upnp.add_port_mapping(port, port, 'TraitorsArena', 'TCP')
			print("port map added to upnp device")
	else: 
		print("no upnp devices found")
	emit_signal("upnpComplete")

func keepHostAlive(id):
	if !keepAlive: return
	yield(aliveTimer, "timeout")
	DBServer.updateHost(gameConfig, id)
	keepHostAlive(id)
# ==============================================

func hostConnected():
	print("Connected to host. Telling host my details...")
	rpc_id(1, "host_registerPlayer", User.profile.getData() )

master func host_registerPlayer(info):
	var id = get_tree().get_rpc_sender_id()
	# TODO SECURITY CHECKS
	# Send the info of all other players back to the client 
	for otherId in players:
		rpc_id(id, "registerPlayer", otherId, players[otherId])
	
	rpc("registerPlayer", id, info) # Send client info to all peers

	if players.size() >= gameConfig["maxPlayers"]: 
		rpc("configureGame", gameConfig)

sync func registerPlayer(id, info):
	print("New peer (id = %s)" % id)
	print("info: ", info)
	players[id] = info
	gameConfig["players"] = players.size()
	emit_signal("playerRegistered", players)

sync func configureGame(config):
	var thisID = get_tree().get_network_unique_id()
	game = load(gameScenePath).instance()
	game.setUp(config, players, thisID)
	rpc_id(1, "host_playerReady", thisID)

master func host_playerReady(id):
	ready_peers.push_back(id)
	# all players AND the host must be ready
	if ready_peers.size() > players.size():
		rpc("startGame")

sync func startGame():
	keepAlive = false
	if is_network_master(): get_tree().get_root().add_child(game)
	else: Globals.setScene(game)
	emit_signal("gameStarted")

func clientDisconnected(id):
	print("Peer (id = %s) disconnected" % id)

func hostDisconnected():
	print("Host disconnected")