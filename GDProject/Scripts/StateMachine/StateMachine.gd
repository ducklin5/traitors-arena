"""
A generic state machine class that will manage and transition between states
"""
extends Node
class_name StateMachine

# keep track of the current state and state history
# MAKE SURE TO SET THE START_STATE
export(String) var START_STATE = "startNode"
var stateStack = []

enum { None, Enter, Exit, Transit }
signal transitioned(type)

const PREV_STATE = "_PREV_STATE_"

"""
State Machine initialization
"""
func _ready():
	stateStack.push_front(START_STATE)
	get_node(stateStack[0]).enter()


"""
function that take in a name of a state under the machine node and switches current processing to it
also updates the stack
Input: nextStateName (string)
Output: operation success (bool)
"""
func changeState(nextState):
	if not nextState is String: return false
	if nextState == "": return false
	# special case: go to the previous state
	if nextState == PREV_STATE:
		if get_node(stateStack[0]).exit() != false: 
			stateStack.pop_front()
			emit_signal("transitioned", Exit)
		else: printerr("Couldnt exit (to previous) from: %s" % stateStack[0])
	else:
		# TODO: error handling if a state node of that name doesnt exist
		if !has_node(nextState): printerr("non-existant State: %s" % nextState)
		var nextStateNode = get_node(nextState)
		# special case: state that uses the state stack, append to the stack
		if nextStateNode.stacks:
			if nextStateNode.enter() != false: 
				stateStack.push_front(nextState)
				emit_signal("transitioned", Enter)
			else: printerr("Couldnt enter stack state : %s" % nextState)
		else:
			if get_node(stateStack[0]).exit() != false:
				if nextStateNode.enter() != false: 
					stateStack[0] = nextState
					emit_signal("transitioned", Transit)
				else: printerr("Couldnt enter : %s" % nextState)
			else: printerr("Couldnt exit : %s" % stateStack[0])
	return true

"""
function to change state properties
"""
func modifyState(state, property, value):
	if find_node(state):
		get_node(state).set(property, value)

"""
function to get the current state node
"""
func getCurrent():
	return get_node(stateStack[0])
	
"""
function that runs the current state's process
"""
func process(delta):
	var nextState = get_node(stateStack[0]).process(delta)
	changeState(nextState)

"""
function that passes input events to the state
"""
func input(event):
	var nextState = get_node(stateStack[0]).handleInput(event)
	changeState(nextState)