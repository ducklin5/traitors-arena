extends Node
class_name State

const PREV_STATE = StateMachine.PREV_STATE
export(bool) var stacks = false
var clock: float = 0.0

"""
Returns the Node that uses the stateMachine
"""
func getHost():
	return get_parent().get_parent()

"""
gets called upon entering the state
Output: bool true if function was succesful
"""
func enter():
	return true

"""
function that gets called by the state machine when a new input is happens
"""
func handleInput(event):
	return event

"""
function that gets called by the state machine regularly
Output: the name of the next state (state)
"""
func process(delta):
	return delta

"""
function that gets called upon leaving the state
bool true if function was succesful
"""
func exit():
	return true