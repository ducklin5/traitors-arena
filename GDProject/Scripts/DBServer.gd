extends Node

var domain = "https://5bf4a813-c64d-4931-b60f-32415197c808-bluemix.cloudant.com/"
var key = "hingearkabilsitifuldstog"
var password = "551a3159b389b86284697a6c50641081af915fff"
var b64Credntials = Marshalls.utf8_to_base64(key+":"+password)
var defaultHeaders = ["Authorization: Basic " + b64Credntials]
var aliveDelay = 10

var updateHttp = HTTPRequest.new()
var getHttp = HTTPRequest.new()

func _ready():
	add_child(updateHttp); 
	add_child(getHttp);

func updateHost(config :Dictionary, docID = null):
	var url = domain + "hosts/_design/hostList/_update/updateHost/"
	if docID: url += docID
	var headers = defaultHeaders + ["Content-Type: application/json"]
	var data = JSON.print(config)
	
	updateHttp.cancel_request()
	updateHttp.request(url, headers, true, HTTPClient.METHOD_POST, data)
	var response = yield(updateHttp, "request_completed")
	
	var body = JSON.parse(response[3].get_string_from_utf8()).result
	return body._id

func getHosts():
	var time = (OS.get_unix_time()-aliveDelay)*1000
	var url = domain + "hosts/_design/hostList/_view/by_revTime?startkey=" + String(time)
	
	getHttp.cancel_request()
	getHttp.request(url, defaultHeaders)
	var response = yield(getHttp, "request_completed")
	
	var body = JSON.parse(response[3].get_string_from_utf8()).result
	var list = {}
	if body.has("rows"):
		for row in body.rows:
			list[row.id] = row.value
	return list