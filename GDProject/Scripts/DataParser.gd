extends Node
var file = File.new()

func loadData(url):
	if url == null: return
	if !file.file_exists(url): return
	file.open(url, File.READ)
	var data = parse_json(file.get_as_text())
	file.close()
	return data

func writeData(url, dict):
	if url == null: return
	file.open(url, File.WRITE)
	file.store_line(to_json(dict))
	file.close()
	return
	