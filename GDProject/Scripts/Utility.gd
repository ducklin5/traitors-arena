extends Node
tool

# Utilitity
static func clampVector(vec :Vector2, lb, ub):
	return clamp(vec.length(), lb , ub) * vec.normalized()

static func avgArray( array :Array):
	var cumsum = 0
	for val in array: cumsum += val
	return cumsum/array.size()

static func normalizeAngle(angle):
	angle = fmod(angle, 2*PI) # -2Pi to 2Pi
	angle = fmod(angle + 2*PI,  2*PI);  # 0 to 2Pi
	if angle > PI: angle -= 2*PI #-Pi to Pi
	return angle

# https://github.com/photonstorm/phaser/issues/2494
static func shortestAngle(from , to):
	var diff = normalizeAngle(to) - normalizeAngle(from)
	var times = floor((diff + PI) / (2*PI))  # balck magic
	return (diff - (times * 2*PI)) * -1

static func merge_dict(target, patch):
	for key in patch:
		target[key] = patch[key]

static func randString(length, pool = range(0,256)):
	var string = ""
	for i in length:
		string += char(pool[randi()%pool.size()])
	return string

static func cRange( start :String, stop :String):
	var startInt :int = start.to_ascii()[0]
	var stopInt :int = stop.to_ascii()[0] + 1
	return range(startInt, stopInt)

static func setControlAM(control:Control, anchors, margins):
	for i in 4: control.set_anchor_and_margin(i, anchors[i], margins[i])

static func create_2d_array(height, width, value):
	var row = []; 
	for x in width: row.append(value)
	var a = []
	for y in height: a.append(row.duplicate())
	return a

static func bspRect(rect:Rect2, minSize:Vector2, iterations = INF)->Array:
	var canVSplit = rect.size.x > minSize.x * 2
	var canHSplit = rect.size.y > minSize.y * 2
	
	if !(canVSplit or canHSplit) or iterations <= 0: return [rect]
	if canVSplit and canHSplit: canVSplit = randi()%2
	var rectA; var rectB;
	
	if canVSplit:
		var widthA = round(rand_range(minSize.x, rect.size.x-minSize.x))
		rectA = Rect2(rect.position.x, rect.position.y, widthA, rect.size.y)
		rectB = Rect2(rect.position.x+widthA, rect.position.y, rect.size.x-widthA, rect.size.y)
	else:
		var heightA = round(rand_range(minSize.y, rect.size.y-minSize.y))
		rectA = Rect2(rect.position.x, rect.position.y, rect.size.x, heightA)
		rectB = Rect2(rect.position.x, rect.position.y+heightA, rect.size.x, rect.size.y-heightA)

	return bspRect(rectA, minSize, iterations-1) + bspRect(rectB, minSize, iterations-1)
