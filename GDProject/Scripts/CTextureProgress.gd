extends Container
class_name CustomTextureProgress
tool

export(Texture) var textureUnder setget setUnderTexture
export(Texture) var textureProgress setget setProgressTexture
export(Texture) var textureOver setget setOverTexture

export(Quat) var stretchMargin setget setStretchMargin
export(Quat) var progressMargin setget setProgMargin

export(Material) var materialUnder setget setUnderMaterial
export(Material) var materialProgress setget setProgressMaterial
export(Material) var materialOver setget setOverMaterial

export(float) var maxValue = 100 setget setMax
export(float) var minValue = 0 setget setMin
export(float) var value = 50 setget setValue

var under = NinePatchRect.new()
var progress = NinePatchRect.new()
var over = NinePatchRect.new()
var layers = [under, progress, over]

func _init():
	for layer in layers:
		layer.anchor_left = 0
		layer.anchor_top = 0
		layer.anchor_right = 1
		layer.anchor_bottom = 1
		add_child(layer)
	updateProgress()

func setStretchMargin(margins :Quat):
	stretchMargin = margins
	for layer in [under, progress, over]: 
		layer.patch_margin_left = margins.x
		layer.patch_margin_top = margins.y
		layer.patch_margin_right = margins.z
		layer.patch_margin_bottom = margins.w
	updateProgress()

func setProgMargin(margins :Quat):
	progressMargin = margins
	updateProgress()

func setUnderTexture(texture):
	textureUnder = texture
	under.texture = texture
	updateProgress()

func setProgressTexture(texture):
	textureProgress = texture
	progress.texture = texture
	updateProgress()

func setOverTexture(texture):
	textureOver = texture
	over.texture = texture
	updateProgress()

func setUnderMaterial(mat):
	materialUnder = mat
	under.material = mat
	updateProgress()

func setProgressMaterial(mat):
	materialProgress = mat
	progress.material = mat
	updateProgress()

func setOverMaterial(mat):
	materialOver = mat
	over.material = mat
	updateProgress()

func setMax(amount):
	maxValue = amount
	setValue(value)

func setMin(amount):
	minValue = amount
	setValue(value)

func setValue(amount):
	value = clamp(amount, minValue, maxValue)
	updateProgress()

func updateProgress():
	var fract = (value - minValue)/(maxValue - minValue)
	progress.patch_margin_left = lerp(progressMargin.x, stretchMargin.x, fract)
	progress.patch_margin_right = lerp(progressMargin.z, stretchMargin.z, fract)
	
	progress.anchor_right = fract
	progress.margin_right = 0
	
	rect_min_size.x = stretchMargin.x + stretchMargin.z
	rect_min_size.y = stretchMargin.y + stretchMargin.w
	update()
