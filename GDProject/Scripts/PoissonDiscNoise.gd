tool
extends Resource
class_name PoissonDiscNoise

export var _seed:int = 0
export var k:int = 2
export var radius = 4
export var width:int = 0 
export var height:int = 0
var unitSize
var cols; var rows;
var unitGrid
var active
var samples setget ,getSamples

func _updateSamples():
	seed(_seed)

	unitSize = radius/sqrt(2)
	cols = ceil(width/unitSize)
	rows = ceil(height/unitSize)
	
	unitGrid = util.create_2d_array(rows,cols,null) 
	active = []
	samples = []
	
	var randCell = Vector2(randi()%width,randi()%height)
	var randUnit = (randCell/unitSize).floor()
	
	unitGrid[randUnit.y][randUnit.x] = randCell
	active.append(randCell)
	
	while active.size():
		var idx = randi()%active.size()
		var pos = active[idx]
		var gotSample = false
		
		for n in k:
			var sample = Vector2(1,0).rotated(randf()*2*PI)
			sample *= rand_range(radius,2*radius)
			sample += pos
			
			if _validSample(sample):
				var sampleUnit = (sample/unitSize).floor()
				unitGrid[sampleUnit.y][sampleUnit.x] = sample
				active.append(sample)
				samples.append(sample)
				gotSample = true

		if !gotSample: active.remove(idx)

func _validSample(sample):
	if (sample.x < 0 or  sample.x >= width 
		or sample.y < 0 or sample.y >= height): return false
	
	var sampleUnit = (sample/unitSize).floor()
	var x = sampleUnit.x; var y = sampleUnit.y
	
	for j in range(y-1, y+2):
		for i in range(x-1, x+2):
			if i < 0 or j < 0 or i >= cols or j >= rows: continue
			var neighbour = unitGrid[j][i]
			if neighbour == null: continue
			if sample.distance_to(neighbour) < radius:
				return false
	return true

func getSamples():
	_updateSamples()
	return samples