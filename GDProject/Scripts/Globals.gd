extends Node

var PORTS = range(27015,27030)
var JOY_RANGE = 100 # max scope distance from player
var JOY_DEAD_ZONE = 0.4 # minimum threshold  (range 0 - 1)
var MOUSE_SCROLL_SENSITIVITY = 0.6
var mouseScrollValue = 0
var releaseActions = ["dir_up", "dir_down", "dir_left", "dir_right", 
					"roll", "run", "attack", "next_weapon", "prev_weapon"]
enum { RIGHT = 8, LEFT = 4, DOWN = 2, UP = 1 }

var current_scene = null

func _ready():
	var root = get_tree().get_root()
	current_scene = root.get_child(root.get_child_count() -1)

func setScene(node:Node):
	call_deferred("_deferred_setScene", node)

func _deferred_setScene(node:Node):
	current_scene.free()
	get_tree().get_root().add_child(node)
	get_tree().set_current_scene(node)
	current_scene = node

func _input(event):
	if event is InputEventMouseButton:
		if event.button_index in [BUTTON_WHEEL_UP, BUTTON_WHEEL_DOWN]:
			if !event.is_pressed():
				get_tree().set_input_as_handled()
				return
			match event.button_index:
				BUTTON_WHEEL_UP: mouseScrollValue += MOUSE_SCROLL_SENSITIVITY
				BUTTON_WHEEL_DOWN: mouseScrollValue -= MOUSE_SCROLL_SENSITIVITY
			if abs(mouseScrollValue) < 1: 
				get_tree().set_input_as_handled()
			else:
				mouseScrollValue = fmod(mouseScrollValue, 1.0)

func _notification(type):
	match type:
		MainLoop.NOTIFICATION_WM_FOCUS_OUT:
			for action in releaseActions:
				Input.action_release(action)
				get_tree().set_input_as_handled()

func pollDirNib(): 
	var dirBtns = 0
	dirBtns += UP * int(Input.is_action_pressed("dir_up"))
	dirBtns += DOWN * int(Input.is_action_pressed("dir_down"))
	dirBtns += LEFT * int(Input.is_action_pressed("dir_left"))
	dirBtns += RIGHT * int(Input.is_action_pressed("dir_right"))
	dirBtns -= (UP + DOWN) * int(dirBtns&UP and dirBtns&DOWN)
	dirBtns -= (LEFT + RIGHT) * int(dirBtns&LEFT and dirBtns&RIGHT)
	return dirBtns

func nibFromAngle(angle):
	angle = rad2deg(angle)
	var nib = 0
	nib += UP * int(angle < -22.5 and angle > -157.5)
	nib += DOWN * int(angle > 22.5 and angle < 157.5)
	nib += LEFT * int(abs(angle) > 112.5)
	nib += RIGHT * int(abs(angle) < 67.5)
	return nib

func vectorFromNib(nib):
	var dirVec = Vector2()
	dirVec.x = int(nib & RIGHT > 0) - int(nib & LEFT > 0)
	dirVec.y = int(nib & DOWN > 0) - int(nib & UP > 0)
	return dirVec.normalized()
