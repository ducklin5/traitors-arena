extends Control

export var hotSlotCount  = 4
var inventory: Inventory
var slots = []
var floatingItem = null

onready var SlotsGrid = $vbox/hbox2/Slots
onready var Hotbar = $vbox/hbox2/Hotbar
onready var ToggleBtn = $vbox/hbox/Toggle

class ItemUI:
	extends TextureRect
	var slot : SlotUI
	
	static func fromItem( item: Item):
		var uiItem = ItemUI.new()
		uiItem.name = "uiItem_" + item.name
		uiItem.texture = item.icon
		return uiItem
	func _init():
		self.expand = true
		self.stretch_mode = TextureRect.STRETCH_KEEP_ASPECT_CENTERED
		self.size_flags_horizontal = SIZE_EXPAND_FILL
		self.size_flags_vertical = SIZE_EXPAND_FILL
		self.mouse_filter = Control.MOUSE_FILTER_IGNORE;

class SlotUI:
	extends TextureRect
	var index
	var uiItem
	var amount :int setget setAmount
	var equipped :bool setget setEquipped
	
	func _init(_index, isHotSlot = false):
		index = _index
		expand = true
		texture = load("res://Assets/slot.png")
		size_flags_horizontal = SIZE_EXPAND_FILL
		size_flags_vertical = SIZE_EXPAND_FILL
		if isHotSlot: modulate = Color.crimson
		var label = Label.new()
		label.name = "amount"
		add_child(label)
		var equipIcon = TextureRect.new()
		equipIcon.name = "equipIcon"
		equipIcon.texture = load("res://Game/Controllers/HUD/equipIcon.tres")
		equipIcon.set_anchors_and_margins_preset(Control.PRESET_BOTTOM_RIGHT)
		equipIcon.hide()
		add_child(equipIcon)
		

	func putItem( _uiItem : ItemUI):
		if _uiItem: removeItem()
		_uiItem.rect_size = self.rect_size
		_uiItem.slot = self
		_uiItem.rect_global_position = Vector2(0, 0)
		add_child(_uiItem)
		uiItem = _uiItem
		
	func removeItem() -> ItemUI:
		if !uiItem: return null
		remove_child(uiItem)
		var temp = uiItem
		uiItem = null
		return temp
	
	func setAmount(newAmount):
		amount = newAmount
		if amount > 1:
			get_node("amount").show()
			get_node("amount").text = String(amount)
		else: get_node("amount").hide()
	
	func setEquipped(value):
		equipped = value
		get_node("equipIcon").visible = equipped

func setup(_inventory: Inventory):
	inventory = _inventory
	inventory.connect("inventoryChanged", self, "updateInventoryUI")	
	for slot in inventory.slots:
		var uiSlot
		if slot is inventory.HotSlot:
			uiSlot = SlotUI.new(slot.index, true)
			Hotbar.add_child(uiSlot)
		else:
			uiSlot = SlotUI.new(slot.index)
			SlotsGrid.add_child(uiSlot)
		slots.append(uiSlot)

func getUiSlot(slotIndex):
	for uiSlot in slots:
		if uiSlot.index == slotIndex:
			return uiSlot
	return null

func updateInventoryUI():
	for slot in inventory.slots:
		var uiSlot :SlotUI = getUiSlot(slot.index)
		uiSlot.removeItem()
		uiSlot.amount = 0
		uiSlot.equipped = (inventory.equippedSlot == slot)
		if !slot.isEmpty():
			var uiItem = ItemUI.fromItem(slot.item())
			uiSlot.putItem(uiItem)
			uiSlot.amount = slot.count()

func _input(event):
	if floatingItem and event is InputEventMouseMotion:
		floatingItem.rect_global_position = Vector2(event.position.x, event.position.y) - floatingItem.get_size()/2 ;

func _gui_input(event):
	if event is InputEventMouseButton:
		var mousePos = get_local_mouse_position()
		var insideGui = true if event.pressed else (mousePos.x >= 0
							&& mousePos.x <= rect_size.x
							&& mousePos.y >= 0 
							&& mousePos.y <= rect_size.y);
		var clickedSlot: SlotUI
		if insideGui:
			for slot in slots:
				var slotMousePos = slot.get_local_mouse_position()
				var isClicked = (slotMousePos.x >= 0
								&& slotMousePos.x <= slot.rect_size.x
								&& slotMousePos.y >= 0 
								&& slotMousePos.y <= slot.rect_size.y);
				if isClicked: 
					clickedSlot = slot
					break
				
		match event.button_index:
			BUTTON_LEFT: 
				handleLeftMouseEvent(event, insideGui, clickedSlot)
			BUTTON_RIGHT:
				handleRightMouseEvent(event, clickedSlot)

func handleLeftMouseEvent(event, insideGui, clickedSlot):
	if event.pressed:
		if clickedSlot and clickedSlot.uiItem and !floatingItem:
			if clickedSlot.amount > 1:
				floatingItem = clickedSlot.uiItem.duplicate()
				floatingItem.slot = clickedSlot
			else:
				floatingItem = clickedSlot.removeItem()
			clickedSlot.amount -= 1
			floatingItem.rect_global_position = Vector2(event.position.x, event.position.y) - floatingItem.get_size()/2
			$floatSpace.add_child(floatingItem)
	elif floatingItem:
		$floatSpace.remove_child(floatingItem)
		var oldSlot = floatingItem.slot
		if clickedSlot:
			oldSlot.removeItem()
			if clickedSlot.uiItem:
				oldSlot.putItem(clickedSlot.removeItem())
			clickedSlot.putItem(floatingItem)
			inventory.requestSwapSlots(oldSlot.index, clickedSlot.index)
		elif !insideGui: inventory.requestRemoveItem(oldSlot.index)
		else:
			oldSlot.putItem(floatingItem)
			oldSlot.amount += 1
		floatingItem = null

func handleRightMouseEvent(event, clickedSlot):
	if event.pressed and clickedSlot:
		if !clickedSlot.equipped:
			inventory.requestEquip(clickedSlot.index)
		else:
			inventory.requestUnequip(clickedSlot.index)
	
