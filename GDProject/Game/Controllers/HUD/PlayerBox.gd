extends VBoxContainer
tool
var player:PlayerState
var actor:Actor
var soul:Actor

func setup( _player: PlayerState ):
	if !_player: return
	player = _player
	player.connect("actorChanged", self, "updateSignals")
	player.connect("valueChanged", self, "updateValues")
	updateSignals(player.actor)
	$Main/vbox/Name.text = player.username
func updateSignals(newActor):
	if actor and !actor.is_queued_for_deletion():
		actor.get_node("Health").disconnect("healthChanged", self, "updateHealth")
		actor = null

	if newActor and newActor != player.soul:
		actor = newActor
		var actorHealth = actor.get_node("Health")
		actorHealth.connect("healthChanged", self, "updateHealth")
		updateHealth(actorHealth.health, actorHealth.healthMax, actorHealth.shield, actorHealth.shieldMax)
	else:
		updateHealth(0, 1, 0, 1)

	if player.soul and !soul:
		soul = player.soul
		var soulHealth = soul.get_node("Health")
		soulHealth.connect("healthChanged", self, "updateSoul")
		updateSoul(soulHealth.health, soulHealth.healthMax, 0, 0)

func updateHealth(health, healthMax, shield, shieldMax):
	var healthBar :CustomTextureProgress = $Main/vbox/Bars/Health
	var progMaterial = healthBar.materialProgress

	healthBar.maxValue = healthMax + shield

	$Tween.interpolate_property(
		healthBar, "value",
		healthBar.value, health + shield,
		0.2,Tween.TRANS_CIRC,Tween.EASE_OUT)

	var total = health + shield
	var shieldFract = float(shield)/total if total else 0
	$Tween.interpolate_property(
		progMaterial, "param/threshold",
		progMaterial.get_shader_param("threshold"), 1-shieldFract,
		0.2,Tween.TRANS_LINEAR,Tween.EASE_OUT)
	$Tween.start()

func updateSoul(health, maxHealth, shield, shieldMax):
	var soulBar :CustomTextureProgress = $Main/vbox/Bars/Soul
	soulBar.maxValue = maxHealth
	$Tween.interpolate_property(
		soulBar, "value",
		soulBar.value, health,
		0.2,Tween.TRANS_CIRC,Tween.EASE_OUT)
	$Tween.start()

func updateValues():
	$"Stats/Tokens/Label".text = player.tokens as String
	$"Stats/Kills/Label".text = player.kills as String