extends PanelContainer

var titles = ["Color", "Kills", "Tokens", "Vessels"]
var teamItems = {}

func _ready():
	$Table.columns = titles.size()
	$Table.set_column_titles_visible(true)
	for i in titles.size():
		$Table.set_column_title(i,titles[i])
	$Table.create_item()
	$Table.hide_root = true

func updateData(teams:Array):
	for team in teamItems:
		if !teams.has(team):
			$Table.get_root().remove_child(teamItems[team])
			teamItems.erase(team)
	for team in teams:
		var item:TreeItem
		if teamItems.has(team): 
			item = teamItems[team]
			
		else:
			item = $Table.create_item()
			teamItems[team] = item
		updateItem(item, team)
	
	teams.sort_custom(Sort, "byTokens")
	for team in teams: teamItems[team].move_to_top()

class Sort:
	static func byTokens(teamA, teamB):
		return teamA.tokens < teamB.tokens


func updateItem(item, team):
	item.set_custom_bg_color(0, team.color)
	item.set_text(1, String(team.kills))
	item.set_text(2, String(team.tokens))
	item.set_text(3, String(team.vessels.size()))