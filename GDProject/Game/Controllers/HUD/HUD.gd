extends CanvasLayer

var player; var map; var teams;

var HUDScale = 1
var overlay: bool setget setOverlay

var invetoryOpened = false
var invetoryPos; var dockedInvPos;

var mapPos; var dockedMapPos;

var usableEntity = null

func _ready():
	if player and map:
		$PlayerUI.player = player
		$InventoryUI.setup(player.inventory)
		$MiniMap.setup(map, player)
	Input.set_mouse_mode(Input.MOUSE_MODE_HIDDEN)
	$InventoryUI.ToggleBtn.connect("button_up", self, "toggleInventory")
	get_tree().get_root().connect("size_changed", self, "resize")
	resize()

func _process(delta):
	$ScopeSprite.position = get_viewport().get_mouse_position()
	updateUsablePos()

func resize():
	var anchor

	var invSize = Vector2(192, 320)* HUDScale
	anchor = get_viewport().size * Vector2(1, 1)
	dockedInvPos = anchor - invSize*Vector2(0.28, 1)
	invetoryPos = anchor - invSize*Vector2(1,1)
	$InventoryUI.rect_size = invSize
	$InventoryUI.rect_position = dockedInvPos


	var dockedMapSize = Vector2(200,200) * HUDScale
	anchor = get_viewport().size * Vector2(1, 0)
	dockedMapPos = anchor - dockedMapSize * Vector2(1,0)
	var mapSize = Vector2(480,480) * HUDScale
	mapPos = get_viewport().size/2 - mapSize/2

	$MiniMap.rect_size = dockedMapSize
	$MiniMap.rect_position = dockedMapPos

func setTime(time):
	time = int(time)
	var seconds = time % 60
	var minutes = int(time/60)
	var hours = int(minutes/60)
	minutes %= 60
	var string = ""
	if hours: string += "%02d:" % hours
	string += "%02d:%02d" % [minutes, seconds]
	$ClockPanel/Label.text = string

func setOverlay(newValue):
	if newValue:$Overlay.show()
	if !overlay and newValue:
		$tOverlay.interpolate_property(
			$Overlay.get_material(), "shader_param/amount",
			0, 0.7,
			0.5, Tween.TRANS_LINEAR, Tween.EASE_IN)
	elif overlay and !newValue:
		$tOverlay.interpolate_property(
			$Overlay.get_material(), "shader_param/amount",
			0.7, 0,
			0.5, Tween.TRANS_LINEAR, Tween.EASE_IN)
	$tOverlay.start()
	yield($tOverlay, "tween_completed")
	if !newValue: $Overlay.hide()
	overlay = newValue

func toggleInventory():
	var currPos = $InventoryUI.rect_position
	if !invetoryOpened:
		setOverlay(true)
		$tInventory.interpolate_property(
			$InventoryUI, "rect_position",
			currPos, invetoryPos,
			0.5, Tween.TRANS_LINEAR, Tween.EASE_IN)
		invetoryOpened = true
	else:
		setOverlay(false)
		$tInventory.interpolate_property(
			$InventoryUI, "rect_position",
			currPos, dockedInvPos,
			0.5, Tween.TRANS_LINEAR, Tween.EASE_IN)
		invetoryOpened = false
	$tInventory.start()
	return invetoryOpened

func updateUsableUI(entity, showPanel, usableButton = null):
	usableEntity = entity
	if entity:
		updateUsablePos()
		if entity is Item:
			updateUsablePanel("Pick Up", entity.displayName, entity.description)
		elif entity is VesselActor:
			updateUsablePanel("Posses", entity.name, "")
		$UsableUI.show()
		if showPanel: $UsableUI/Panel.show()
		else: $UsableUI/Panel.hide()
	else:
		$UsableUI/Panel.hide()
		$UsableUI.hide()

func updateUsablePanel(action, displayName, description):
	$UsableUI/Panel/hbox/vbox/Action.text = action
	$UsableUI/Panel/hbox/vbox/Name.text = displayName
	$UsableUI/Panel/hbox/vbox/Description.text = description

func updateUsablePos():
	if usableEntity:
		var entityPos = usableEntity.get_global_transform_with_canvas().get_origin()
		$UsableUI.rect_position = entityPos

func toggleStatsUI():
	if $StatsUI.visible:
		$StatsUI.hide()
	else:
		$StatsUI.updateData(teams)
		$StatsUI.show()