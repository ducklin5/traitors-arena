extends VBoxContainer

const PlayerBox = preload("res://Game/Controllers/HUD/PlayerBox.tscn")

var player setget setPlayer
var team setget setTeam

func setPlayer(_player):
	player = _player
	$PlayerBox.setup(_player)
	player.connect("actorChanged", self, "actorChanged")


func actorChanged(newActor):
	if "team" in newActor: setTeam(newActor.team)
	else: setTeam(null)

func setTeam(_team):
	if team: team.disconnect("playerChanged", self, "updateUI")
	team = _team
	if team: team.connect("playerChanged", self, "updateUI")
	updateUI()

func updateUI():
	for node in $hbox/Squad.get_children():
		$hbox/Squad.remove_child(node)
	
	if team: 
		$hbox/Team.color = team.color
	else: 
		$hbox/Team.color = Color(0.5,0.5,0.5)
		return
		
	for vessel in team.vessels:
		if vessel.player and vessel.player != player:
			var playerBox = PlayerBox.instance()
			playerBox.setup(vessel.player)
			playerBox.get_node("Stats").hide()
			$hbox/Squad.add_child(playerBox)