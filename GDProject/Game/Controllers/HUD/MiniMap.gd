extends Panel

onready var viewport = $ViewportContainer/Viewport
onready var BG = $ViewportContainer/Viewport/BG
onready var camera = $ViewportContainer/Viewport/Camera2D
onready var Icons = $ViewportContainer/Viewport/Icons

var player:PlayerState
var team:Team
var vesselMarkers = {}



func setup(map:Map, _player:PlayerState):
	for vname in ["Terrain", "Houses"]:
		BG.add_child(map.getView(vname).duplicate())
	player = _player
	player.connect("actorChanged", self, "_on_actorChanged")

func _on_actorChanged(newActor):
	if team:
		team.disconnect("playerChanged", self, "updateMarkers")
		team = null
	if "team" in player.actor:
		team = player.actor.team
		team.connect("playerChanged", self, "updateMarkers")
	updateMarkers()

func updateMarkers():
	for marker in vesselMarkers.values():
		marker.queue_free()
	vesselMarkers = {}
	if !team: return
	for vessel in team.vessels:
		var marker = Sprite.new()
		marker.texture = load("res://Assets/Icons/TraitorsArena_Icon.png")
		marker.scale = Vector2(0.3,0.3)
		vesselMarkers[vessel] = marker
		Icons.add_child(marker)

func _process(delta):
	if player and player.actor:
		camera.position = player.actor.position
	for vessel in vesselMarkers:
		var marker = vesselMarkers[vessel]
		var pos = vessel.position
		var rect = getCameraRect()
		marker.position.x = clamp(pos.x, rect.position.x, rect.end.x)
		marker.position.y = clamp(pos.y, rect.position.y, rect.end.y)

func getCameraRect():
	var ctrans = camera.get_canvas_transform()
	var min_pos = -ctrans.get_origin() / ctrans.get_scale()
	var view_size = viewport.size / ctrans.get_scale()
	return Rect2(min_pos, view_size)
