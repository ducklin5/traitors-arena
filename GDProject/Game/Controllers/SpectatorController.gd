extends Controller
class_name SpectatorController

enum {FREE, PLAYER, TEAM}

var mode = FREE setget setMode

func _init(camera).(camera): pass

func setMode(newMode):
	mode = newMode

func _ready(): cameraSpeed = 400

func processInputs(delta):
	var dirVect = Globals.vectorFromNib(dirBtns)
	camera.position += dirVect * cameraSpeed * delta

func _input(event):
	if event is InputEventMouseButton:
		var zoom = camera.zoom.x
		match event.button_index:
			BUTTON_LEFT: zoom -= 0.1
			BUTTON_RIGHT: zoom += 0.1
		zoom = clamp(zoom,0.5,1.5)
		camera.zoom = Vector2(zoom,zoom)
