extends Controller
class_name PlayerController

const HUD = preload("res://Game/Controllers/HUD/HUD.tscn")
var hud = HUD.instance()
var player :PlayerState
var actor :Actor
var usableEntity = null

func _init(camera:Camera2D, _player:PlayerState, map:Map, teams:Array).(camera):
	player = _player
	player.connect("actorChanged", self, "actorChanged")
	player.connect("usablesChanged", self, "updateUsableEntity")
	camera.zoom = Vector2(0.75,0.75)
	camera.position = player.soul.position.round()
	hud.player = player
	hud.map = map
	hud.teams = teams
	add_child(hud)

func _unhandled_input(event):
	if event is InputEventMouseMotion:
		updateUsableEntity()
	
	for action in InputMap.get_actions():
		if event.is_action_pressed(action):
			match action:
				"prev_weapon": actions[action] = true
				"next_weapon": actions[action] = true
				"exit_vessel": player.requestPosses(player.soul)
				"bag": hud.toggleInventory()
				"stats": hud.toggleStatsUI()
				"hit":
					var actorHealth = player.actor.get_node("Health")
					actorHealth.rpc("damage", 30)
				"use":
					if !usableEntity: continue
					elif usableEntity is VesselActor:
						player.requestPosses(usableEntity)
					elif usableEntity is Item:
						player.requestPickItem(usableEntity)
				"primary":
					var item = player.inventory.activeItem()
					if item: item.use(0)

		elif event.is_action_released(action):
			match action:
				"primary":
					var item = player.inventory.activeItem()
					if item: item.use(1)
				"stats": hud.toggleStatsUI()
func pollActions():
	for action in InputMap.get_actions():
		if Input.is_action_pressed(action): 
			match action:
				"attack": actions[action] = cursorPos
				"roll": actions[action] = dirBtns
				"prev_weapon", "next_weapon": pass
				_: actions[action] = true

func processInputs(delta):
	hud.setTime(time)
	if actor:
		actor.dirBtns = dirBtns
		actor.cursorPos = cursorPos
		util.merge_dict(actor.actions, actions)
		camera.position = actor.position
	camera.offset = (cursorPos/20).round()
	
func actorChanged(newActor):
	actor = null
	var from = camera.position
	var to = newActor.position
	var dur = (to - from).length() / cameraSpeed
	if dur:
		tween.interpolate_property(camera, 'position', from, to, dur, Tween.TRANS_LINEAR, Tween.EASE_OUT)
		tween.start()
		yield(tween, "tween_completed")
	actor = newActor

func updateUsableEntity():
	usableEntity = null
	var showPanel = false
	var closestDist = -1
	for entity in player.usables:
		var dist = entity.get_local_mouse_position().length()
		if closestDist < 0 or dist < closestDist:
			closestDist = dist
			usableEntity = entity
			showPanel =  dist < 100
	$HUD.updateUsableUI(usableEntity, showPanel, "E")
