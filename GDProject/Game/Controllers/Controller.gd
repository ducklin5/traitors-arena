extends Node
class_name Controller

var camera:Camera2D
var tween:Tween = Tween.new()
var cameraSpeed:float = 200

var dirBtns = 0 # binary button state: 0b up down left right
var cursorPos = Vector2(0,0)
var actions = {}
var time

func _init(_camera:Camera2D):
	camera = _camera
	camera.drag_margin_h_enabled = false
	camera.drag_margin_v_enabled = false
	camera.make_current()
	tween.name = 'tCamera'
	add_child(tween)

func _process(delta):
	pollInputs()
	processInputs(delta)
	actions = {}

func pollInputs():
	# dirBtns
	dirBtns = Globals.pollDirNib()
	
	# cursorPos
	# Wrap cursor to Joystick psition
	var joyVect = Vector2()
	joyVect.x = Input.get_joy_axis(0,JOY_AXIS_2)
	joyVect.y = Input.get_joy_axis(0,JOY_AXIS_3)
	joyVect = util.clampVector(joyVect, 0, 1)
	if joyVect.length() > Globals.JOY_DEAD_ZONE:
		Input.warp_mouse_position(get_viewport().size/2 + joyVect * Globals.JOY_RANGE)
	
	cursorPos = get_viewport().get_mouse_position() - get_viewport().size/2
	pollActions()

func pollActions(): pass

func processInputs(delta): pass
