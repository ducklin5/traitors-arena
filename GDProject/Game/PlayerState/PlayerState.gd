extends Node
class_name PlayerState

var peerId
var username
var latency
var actor :Actor
var soul :Actor
var inventory :Inventory
var team :Team setget ,getTeam
var usables : Array = []
var kills:int = 0
var tokens:int = 0
signal actorChanged(newActor)
signal usablesChanged()
signal valueChanged()

func addKill(_tokens):
	kills += 1
	tokens += _tokens
	emit_signal("valueChanged")
	if is_network_master(): rpc("setRemoteValues", kills, tokens)

puppet func setRemoteValues(_kills, _tokens):
	if get_tree().get_rpc_sender_id() == 1:
		kills = _kills
		tokens = _tokens
		emit_signal("valueChanged")

func getTeam():
	if actor is VesselActor:
		return actor.team
	return null

func _init(  _peerId :int, _username :String, _soul :Actor, invSlots :int = 8, hotSlots :int = 4):
	peerId = _peerId
	username = _username
	soul = _soul
	inventory = Inventory.new(peerId, invSlots, hotSlots)
	inventory.name = "Inventory"
	add_child(inventory)
	inventory.connect("itemAdded", self, "pickItem")
	inventory.connect("equipSlot", self, "equipSlot")

func _ready():
	masterPossesSoul()

func masterPossesSoul():
	if !is_network_master(): return
	posses(soul.get_path())

func requestPosses(newActor :Actor):
	if newActor == actor : return
	var path = newActor.get_path()
	if path: rpc_id(1, "posses", path, true)

master func posses(path:String, is_rpc = false):
	if is_rpc and get_tree().get_rpc_sender_id() != peerId: return
	var newActor :Actor = get_node(path)
	if !newActor: return 
	if newActor.player and newActor.player != self : return
	if newActor.state in ["dead","terminate"]: return
	
	if actor: actor.rpc("disable", actor != soul)
	newActor.rpc("enable", get_path(),  actor.get_path() if actor else null)
	yield(newActor, "doneTick")
	rpc("setActor", path)
	
puppetsync func setActor(path):
	if !is_network_master() and get_tree().get_rpc_sender_id() != 1: return
	
	inventory.unequip()
	actor = get_node(path)
	
	usables = []
	emit_signal("usablesChanged")
	emit_signal("actorChanged", actor)

func updateUsables(entity: Node2D, entered: bool):
	if  entered:
		if entity is Actor and entity.player: return 
		if entity is VesselActor:
			if actor is VesselActor or entity.state in ["dead","terminate"]: return
		if entity is Item and !entity.is_in_group("interact"): return
		usables.push_back(entity)
	elif usables.has(entity):
		usables.erase(entity)
	emit_signal("usablesChanged")

func requestPickItem(item :Item):
	if item.is_inside_tree():
		var path = item.get_path()
		inventory.rpc_id(1, "addItem", path, true)
	else: print(item, "is not in the tree")

func equipSlot(slotId):
	if actor is VesselActor:
		rpc("client_equipSlot", slotId)

sync func client_equipSlot(slotId):
	if get_tree().get_rpc_sender_id() != 1 and !is_network_master(): return
	var slot = inventory.slots[slotId]
	var item = slot.item()
	item.do("equip")
	inventory.equippedSlot = slot

func pickItem(item:Item):
	item.do("pick", [self])
