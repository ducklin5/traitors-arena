class_name Team

var id: int
var color: Color
var vessels = [] # dictionary of vessels and their playerStates
var tokens setget ,getTokens
var kills setget ,getKills
signal playerChanged()

func _init(_id:int, _color:Color):
	self.id = _id
	self.color = _color

func addVessel( vessel ):
	vessel.name = String(vessels.size()) + "_" + String(id) + "_vessel"
	vessels.append(vessel)
	vessel.team = self
	vessel.connect("playerChanged", self, "emitPlayerChanged")

func emitPlayerChanged():
	emit_signal("playerChanged")

func getTokens():
	var tokens = 0
	for vessel in vessels:
		if vessel.player:
			tokens += vessel.player.tokens
	return tokens

func getKills():
	var kills = 0
	for vessel in vessels:
		if vessel.player:
			kills += vessel.player.kills
	return kills

func eliminate():
	for vessel in vessels:
		vessel.rpc("terminate")