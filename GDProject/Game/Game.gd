extends Node
class_name Game

const Map = preload("res://Entities/Maps/Map.tscn")
const VesselActor = preload("res://Entities/Actors/VesselActor/VesselActor.tscn")
const SoulActor = preload("res://Entities/Actors/SoulActor/SoulActor.tscn")

var lastUniqueId = -1
# {{ timing
var srvTickRate = 40
var tickRate = 32
var pingRate = 4
var time = 0.0
# }}

# {{ Team info
var teams = []
# }}

# {{ player info
var playerStates = {}
var localController :Controller
var localPlayer :PlayerState
# }}

# {{ Viewport and Map
var viewport :Viewport
var camera :Camera2D
var map: Map
# spwanPoints
var vesselSPs: Array
var soulSPs: Array
# }}

# {{ signals
signal tick(time, latency)
# }}

func setUp(settings, players, thisId):
	var noTeams = settings["teams"]
	var vpTeam = settings["vesselsPerTeam"]
	var _seed = settings["seed"]
	seed(_seed)
	setUpViewport(thisId)
	setUpMap(_seed)
	setUpTeams(noTeams, vpTeam, thisId)
	setUpPlayers(players, thisId)
	setUpController(thisId)

func setUpViewport(thisId):
	var container
	if thisId == 1:
		container = Control.new()
	else:
		container = ViewportContainer.new()  
		container.stretch = true
		container.set_anchors_and_margins_preset(Control.PRESET_WIDE)
		container.mouse_filter = Control.MOUSE_FILTER_IGNORE
	viewport = Viewport.new()
	camera = Camera2D.new()
	camera.current = true
	container.name = "ViewportContainer"
	viewport.name = "Viewport"
	camera.name = "Camera2D"
	viewport.add_child(camera)
	container.add_child(viewport)
	add_child(container)

func setUpMap(_seed):
	map = Map.instance()
	map._seed = _seed
	viewport.add_child(map)
	vesselSPs = map.get_node("SpawnPoints").get_children()
	soulSPs = map.get_node("SpawnPoints").get_children()

func setUpTeams(noTeams, vpTeam, thisId):
	var hueOff = randf()
	for i in noTeams:
		var hue = fmod(hueOff + i/noTeams, 1.0)
		var teamColor = Color.from_hsv(hue,0.8,0.7)
		var team = Team.new(i, teamColor)
		teams.append(team)
		for j in vpTeam:
			var idx = int(team.id*vpTeam+j)%vesselSPs.size()
			var pos = vesselSPs[idx].position
			var vessel = spawnVessel(thisId == 1, pos)
			team.addVessel(vessel)

func spawnVessel(server, pos):
	var vessel = VesselActor.instance()
	vessel.position = pos
	vessel.type = Actor.Server if server else Actor.Client
	map.addEntity(vessel)
	connect("tick", vessel, "tick")
	return vessel

func setUpPlayers(players, thisId):
	var i = 0
	for id in players:
		# create a soul first\
		var pos = soulSPs[i%soulSPs.size()].position
		var soul = spawnSoul(id, thisId == 1, pos)
		# add a player state
		var player = PlayerState.new(id, players[id]["name"], soul)
		player.name = String(id) + "_player"
		playerStates[id] = player
		add_child(player)
		if(thisId == id): localPlayer = player
		i += 1

func spawnSoul(id, server, pos):
	var soul = SoulActor.instance()
	soul.name = String(id) + "_soul"
	soul.type = soul.Server if server else soul.Client
	soul.position = pos
	map.addEntity(soul)
	connect("tick", soul, "tick")
	return soul

func setUpController(thisId):
	#if thisId == 1: return
	if localPlayer: localController = PlayerController.new(camera, localPlayer, map, teams)
	else: localController = SpectatorController.new(camera)
	localController.name = String(thisId) + "_controller"
	add_child(localController)

func _ready():
	Console.register('addItem', {
		'args': [TYPE_INT, TYPE_REAL, TYPE_REAL],
		'description': 'adds an item to the world for every client',
		'target': [self, 'masterAddItem'],
	})
	$tickTimer.wait_time = 1.0 / ( srvTickRate if is_network_master() else tickRate)
	$pingTimer.wait_time = 1.0 / pingRate
	$tickTimer.connect("timeout", self, "emitTickSignal")
	if is_network_master():
		for i in 5:
			var pos = vesselSPs[i].position
			masterAddItem(1,pos.x,pos.y)
		$pingTimer.connect("timeout", self, "sendPings")
		$pingTimer.start()

func _process(delta):
	time += delta
	if localController:
		localController.time = time
	$GameState.process(delta)

# {{ Net Tick
func emitTickSignal():
	var latency = localPlayer.latency if localPlayer else 0
	emit_signal("tick", time, latency)
# }}

# {{ PING
func sendPings():
	rpc_unreliable('ping', time)
	for id in playerStates:
		if playerStates[id].latency:
			rpc_unreliable_id(id, "recieveTime", time, playerStates[id].latency)

puppet func ping(pingTime):
	if get_tree().get_rpc_sender_id() != 1: return
	rpc_unreliable_id(1, 'pong', pingTime)

master func pong(pingTime):
	var id = get_tree().get_rpc_sender_id()
	playerStates[id].latency = time - pingTime

puppet func recieveTime(srvTime, latency):
	if get_tree().get_rpc_sender_id() != 1: return
	localPlayer.latency = latency
	time = srvTime + latency/2
# }}

func masterAddItem(id,x,y):
	if is_network_master():
		lastUniqueId += 1
		rpc("addItem", id, lastUniqueId, x, y)
	else: Console.Log.warn("This is not the server. Command denied")

sync func addItem(id, uniqueId, x, y):
	if get_tree().get_rpc_sender_id() != 1 and !is_network_master(): return
	var item = ItemFactory.createItem(id)
	item.position = Vector2(x,y)
	item.name = item.displayName + String(uniqueId)
	map.addEntity(item)

func masterEliminateTeam():
	if !is_network_master(): return
	if teams.size() <= 2: $eliminateTimer.stop()
	var lowestTeam:Team = teams[0]
	for team in teams:
		if team.tokens <= lowestTeam.tokens:
			lowestTeam = team
	lowestTeam.eliminate()
	rpc("removeTeam", lowestTeam.id)

sync func removeTeam(id):
	if get_tree().get_rpc_sender_id() != 1 and !is_network_master(): return
	for team in teams:
		if team.id == id:
			teams.erase(team)
			return