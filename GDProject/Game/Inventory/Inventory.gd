extends Node
class_name Inventory
signal inventoryChanged()
signal itemAdded(item)
signal equipSlot(slotId)

var peerId = 0
export var invSlots = 5
export var hotSlots = 5
var equippedSlot :Slot = null setget setEquippedSlot
var slots = []

class Slot:
	var index
	var items = []
	
	func _init(_index):
		index = _index
		
	func addItem(item :Item):
		items.append(item)

	func removeItem():
		var output = items.pop_front()
		return output
		
	func isEmpty():
		return items == []
		
	func count(): return items.size()
	
	func item() -> Item:
		if isEmpty(): return null
		return items[0]
		
	func hasItem(item :Item):
		if isEmpty(): return false
		return item().id == item.id

class HotSlot:
	extends Slot
	var hotkey
	
	func _init(_index).(_index):
		pass

func _init( _peerId :int, _invSlots :int, _hotSlots :int = 0):
	peerId = _peerId
	invSlots = _invSlots
	hotSlots = _hotSlots
	for i in invSlots: slots.append(Slot.new(i))
	for j in hotSlots: slots.append(HotSlot.new(invSlots+j))

func SetItemSlot( item, slot ):
	if slot.isEmpty(): slot.setItem(item)

func equipNext():
	#search for next weapon in the inventory
	pass
	
func firstEmptySlot() -> Slot:
	for slot in slots:
		if slot.isEmpty(): return slot
	return null

func getEquipped():
	return

func getSlotWithItem( item ) -> Slot:
	var foundSlot = null
	for slot in slots:
		if slot.hasItem(item):
			foundSlot = slot
			break
	return foundSlot

# {{ Add an Item
master func addItem( path:String, is_rpc = false):
	if is_rpc and get_tree().get_rpc_sender_id() != peerId: return
	var item :Item = get_node(path)
	if !item: 
		print("no such item: ", path)
		return
	var slot :Slot = null
	if item.stacks: slot = getSlotWithItem(item)
	if !slot: slot = firstEmptySlot()
	if slot: slot.addItem(item)
	else: return
	emit_signal("itemAdded", item)
	emit_signal("inventoryChanged")
	if is_rpc: rpc("client_addItem", path)
	
puppet func client_addItem(path):
	if get_tree().get_rpc_sender_id() != 1: return
	addItem(path)
# }}

# {{ Remove Item
func requestRemoveItem(slotId):
	rpc_id(1, "removeItem", slotId, true)

master func removeItem(slotId, is_rpc = false):
	var slot = slots[slotId]
	toggleEquip(slotId, false)
	var item = slot.removeItem()
	if item:
		item.do("drop")
		emit_signal("inventoryChanged")
		if is_rpc: rpc("client_removeItem", slotId)

puppet func client_removeItem(slotId):
	if get_tree().get_rpc_sender_id() != 1: return
	removeItem(slotId)
# }}

# {{ Swap slots 
func requestSwapSlots(indexA, indexB):
	var indices 
	if (indexA >= 0 and indexA < slots.size() and indexB >= 0 and indexB <= slots.size()):
		rpc_id(1, "swapSlots", indexA, indexB, true)

master func swapSlots(indexA, indexB, is_rpc = false):
	var slotA = slots[indexA]
	var slotB = slots[indexB]
	slotA.index = indexB
	slots[indexB] = slotA
	slotB.index = indexA
	slots[indexA] = slotB
	emit_signal("inventoryChanged")
	if is_rpc: rpc("client_swapSlots", indexA, indexB)

puppet func client_swapSlots(indexA, indexB):
	if get_tree().get_rpc_sender_id() != 1: return
	swapSlots(indexA, indexB)
# }}

#{{ Equip an item
func requestEquip(slotId):
	var item = slots[slotId].item()
	if item and item.is_in_group("equip"):
		rpc_id(1, "toggleEquip", slotId, true)

func requestUnequip(slotId):
	if equippedSlot == slots[slotId]:
		rpc_id(1, "toggleEquip", slotId, false)

master func toggleEquip(slotId, value):
	if value:
		var item = slots[slotId].item()
		if item and item.is_in_group("equip"):
			if equippedSlot: 
				toggleEquip(equippedSlot.index, false)
			emit_signal("equipSlot", slotId)
	elif equippedSlot == slots[slotId]:
		rpc("unequip")

func setEquippedSlot(slot):
	equippedSlot = slot
	emit_signal("inventoryChanged")

sync func unequip():
	if get_tree().get_rpc_sender_id() != 1 and !is_network_master(): return
	if equippedSlot:
		var item = equippedSlot.item()
		item.do("unequip")
		setEquippedSlot(null)

func activeItem() -> Item:
	if equippedSlot:
		return equippedSlot.item()
	return null
# }}
