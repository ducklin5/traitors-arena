extends MapView
tool

export(Resource) var noise = PoissonDiscNoise.new()

func _run():
	# remove old spawn points
	var SP = map.get_node("SpawnPoints")
	
	var terrain = map.getView("Terrain")
	
	noise._seed = map._seed
	noise.width = int(map.size.x)
	noise.height = int(map.size.y)
	var cells = noise.getSamples()
	
	for cell in cells:
		if terrain.getColor(cell.x, cell.y) == terrain.BEACH:
			setColor(cell.x, cell.y, Color(0.65,0,0.65))
			
			var pos2d = Position2D.new();
			pos2d.position = cell*map.cell_size
			SP.add_child(pos2d)
