tool
extends MapView

export var seedOff = 0
export var noise :OpenSimplexNoise

func _run():
	noise.seed = map._seed + seedOff
	var val; var color;
	for y in ys:
		for x in xs:
			val = (noise.get_noise_2d(x,y) + 1)/2
			setColor(x,y,Color(val,val,val))
