extends MapView
tool

var beachTiles = []

var WATER = Color("56DFF7")
var BEACH = Color("EAAF62")
var GRASS = Color("7FF49A")
var FOREST = Color("5DB271")
var SOIL = Color("ae9b7d")
var LAKE = Color("16DFC7")

func _run():
	beachTiles = []
	var islandView = map.getView("Island")
	var moistureView = map.getView("Moisture")
	for y in ys:
		for x in xs:
			var z = islandView.getColor(x,y).r
			var m = moistureView.getColor(x,y).r
			
			var color = SOIL
			
			if z < 0.05: color = WATER 
			elif z < 0.15:
				color = BEACH
				beachTiles.append(Vector2(x,y))
			elif z < 0.35:
				if m > 0.35: color = GRASS
				else: color = SOIL
			else:
				if m > 0.3: color = FOREST
				elif m > 0.25: color = GRASS
				else: color = SOIL
			
			if z > 0.5 and m > 0.6: color = LAKE
		
			setColor(x,y,color)
	#islandView.clear(); moistureView.clear();
