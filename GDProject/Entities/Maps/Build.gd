extends MapView
tool

func _run():
	var t = map.getView("Terrain")
	var h = map.getView("Houses")
	var trees = map.getView("Trees")
	
	var l = []
	for i in 5: l.append(map.addLayer())
	
	for y in ys: for x in xs:
		var hColor =  h.getColor(x,y)
		var tColor = hColor if hColor.a != 0 else t.getColor(x,y)
		var isTree = trees.getColor(x,y) != Color(0,0,0,0)
		
		if tColor == t.WATER: 
			l[0].set_cell(x,y,11);
		else: 
			l[0].set_cell(x,y,1+randi()%2)
			if tColor != t.BEACH:
				l[1].set_cell(x,y,8)
				match tColor:
					t.LAKE: l[2].set_cell(x,y,5)
					t.GRASS, t.FOREST: 
						l[2].set_cell(x,y,4)
						if tColor == t.FOREST:
							l[3].set_cell(x,y,7)
							if isTree: l[4].set_cell(x,y,10)
	
	for cell in h.tiles:
		match h.tiles[cell]:
			"ground":
				if randf() < 0.9: l[3].set_cellv(cell, 26)
				else: l[3].set_cellv(cell, 27)
			"wall":
				var adjacent = [cell+Vector2(0,-1), cell+Vector2(0,1),
							cell+Vector2(-1,0), cell+Vector2(1,0),
							cell+Vector2(-1,-1), cell+Vector2(1,-1),
							cell+Vector2(-1,1), cell+Vector2(1,1)]
				var states = [0,0,0,0,0,0,0,0]
				for i in adjacent.size():
					if h.tiles.has(adjacent[i]):match h.tiles[adjacent[i]]:
						"ground": states[i] = 0
						"wall": states[i] = 1
					else: states[i] = 1
				match states:
					[1,0,1,1,..]: l[4].set_cellv(cell, 18) #Top
					[0,1,1,1,..]: l[4].set_cellv(cell, 22) #Bot
					[1,1,1,0,..]: l[4].set_cellv(cell, 14) #Left
					[1,1,0,1,..]: l[4].set_cellv(cell, 15)	#Right
					[1,1,1,1,1,1,1,0]: l[4].set_cellv(cell, 12) #top left
					[1,1,1,1,1,1,0,1]: l[4].set_cellv(cell, 13) #top right
					[1,1,1,1,1,0,1,1]: l[4].set_cellv(cell, 16) #bot left
					[1,1,1,1,0,1,1,1]: l[4].set_cellv(cell, 17) #bot right
					[_,0,0,_,_,_,0,_]: l[4].set_cellv(cell, 24) #top left in wide
					[_,0,_,0,_,_,_,0]: l[4].set_cellv(cell, 25) #top right in wide
					[0,_,0,_,0,_,_,_]: l[4].set_cellv(cell, 21) #bot left in wide
					[0,_,_,0,_,0,_,_]: l[4].set_cellv(cell, 23) #bot right in wide
	for layer in l: layer.update_bitmask_region()
	#map.getMod().visible = true
