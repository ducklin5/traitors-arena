tool
extends MapView

var uvCenter = Vector2(0.5, 0.5)

func _run():
	for y in ys:
		for x in xs:
			var d = (Vector2(x,y) - map.size*uvCenter).length()
			d /= (map.size.x/2)
			setColor(x,y,Color(d,d,d))
