extends Node2D
class_name Map
tool

export(Vector2) var cell_size = Vector2(32,32) setget setCellSize
export(TileSet) var tileset setget setTileSet
export(Vector2) var size = Vector2(200, 200) setget setSize
export(int) var _seed = 0 setget setSeed;
export(int) var quadrant_size = 8
export(Rect2) var chunk = Rect2(0,0,200,200) setget setChunk
export(bool) var cleared = true setget clear
export(bool) var rebuild = false setget  build

signal mapRebuilt

func setCellSize(cellSize):
	cell_size = cellSize
	updateViews()

func setSize(newSize):
	newSize = newSize.round()
	if newSize - size:
		size = newSize
		updateViews()

func setTileSet(newSet):
	tileset = newSet
	
func setChunk(newChunk):
	chunk.position = newChunk.position.round()
	chunk.size = newChunk.size.round()
	updateViews()

func setSeed(val):
	_seed = val
	updateViews()

func updateViews():
	rebuild = false
	property_list_changed_notify()
	if has_node("Views"):
		var start = OS.get_ticks_msec()
		for view in $Views.get_children():
			view.run(self)
		var dur = OS.get_ticks_msec() - start
		print("updateViews time: " + str(dur))
		cleared = false
		emit_signal("mapRebuilt")
	rebuild = true
	property_list_changed_notify()
	
func clear(val=null):
	if !has_node("Views"): return
	for view in $Views.get_children(): view.clear()
	for layer in $Layers.get_children(): layer.free()
	for pos2d in $SpawnPoints.get_children(): pos2d.free()
	cleared = true

func build(val):
	if rebuild: updateViews()

func addLayer():
	var layer = TileMap.new()
	layer.cell_y_sort = true
	layer.cell_size = cell_size
	layer.tile_set = tileset
	layer.cell_quadrant_size = quadrant_size
	layer.name = $Layers.get_child_count() as String
	$Layers.add_child(layer)
	return layer

func addEntity(node):
	var layers = $Layers.get_child_count()
	var topLayer = $Layers.get_child(layers-1)
	topLayer.add_child(node)

func getView(viewName):
	if $Views.has_node(viewName):
		return $Views.get_node(viewName)
	return null

func getMod(): return $CanvasModulate
