extends MapView
tool

func _run():
	var dView = map.getView("Distance")
	var eView = map.getView("Elevation")
	for y in ys:
		for x in xs:
			var d = dView.getColor(x,y).r
			var e = eView.getColor(x,y).r
			var z = lower(d) + e * (upper(d) - lower(d))
			setColor(x,y,Color(z,z,z))
	dView.clear(); eView.clear();

#lowerBound
func lower(d):
	return (1-2*d)*0.2

#upperBound
func upper(d):
	return cos(d*PI/2)
