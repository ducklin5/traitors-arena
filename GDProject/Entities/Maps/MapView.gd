tool
extends Sprite
class_name MapView

export var enabled = true
var map #current map it is working for
var dyImage:Image #view output
var yoff; var xoff;
var ys; var xs;

func _init():
	texture = ImageTexture.new()
	texture.flags = 0 
	centered = false
	
func run(extMap=null):
	if !enabled: return 
	if extMap: map = extMap
	elif !map: return
	if!dyImage: dyImage = Image.new()
	dyImage.unlock()
	dyImage.create(map.chunk.size.x, map.chunk.size.y, false, Image.FORMAT_RGBA8)
	dyImage.lock()
	yoff = map.chunk.position.y
	xoff = map.chunk.position.x
	ys = range(yoff, map.chunk.end.y)
	xs = range(xoff, map.chunk.end.x)
	_run()
	texture.create_from_image(dyImage,0)
	scale = map.cell_size
	offset = map.chunk.position
	update()

func _run():
	pass

func clear():
	texture = ImageTexture.new()
	dyImage = null

func validPixel(x,y):
	return y>=yoff and y<map.chunk.end.y and x>=xoff and x<map.chunk.end.x

func setColor(x,y,color):
	if validPixel(x,y): dyImage.set_pixel(x-xoff,y-yoff, color)

func getColor(x,y)->Color:
	if validPixel(x,y): return dyImage.get_pixel(x-xoff,y-yoff)
	else: return Color(0,0,0,0)
