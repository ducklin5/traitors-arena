shader_type canvas_item;
render_mode blend_disabled;

uniform vec2 tileSize;
uniform sampler2D depthTex;
uniform sampler2D normalTex;

varying vec2 vert;
void vertex(){
	vert=VERTEX;
	vert.y-=tileSize.y;
}
void fragment(){
	COLOR=texture(TEXTURE, UV);
	NORMALMAP=texture(normalTex,UV).rgb;
}

void light(){
	float depth = 0.0;
	vec3 pos3d = vec3(vert.x, -depth*32.0, -vert.y-depth*96.0);
	vec3 light3d = vec3(vert-LIGHT_VEC, LIGHT_HEIGHT);
	
	vec3 toLight = normalize(light3d-pos3d);
	vec3 normal = normalize(NORMAL.xzy);
	normal.x *= -1.0;
	
	float NdotL = max(0.0, dot(normal, toLight));
	LIGHT = vec4(vec3(NdotL), 1.0) * 2.0;
	LIGHT_VEC.y += 10.0;
	//SHADOW_VEC.y+= pos3d.z;
}