tool
extends MapView

export(Resource) var noise = PoissonDiscNoise.new()

func _run():
	var terrain = map.getView("Terrain")
	var houses = map.getView("Houses")
	
	noise._seed = map._seed
	noise.width = int(map.size.x)
	noise.height = int(map.size.y)
	var cells = noise.getSamples()
	
	for cell in cells:
		var tColor =  terrain.getColor(cell.x, cell.y)
		var hColor =  houses.getColor(cell.x, cell.y)
		if tColor == terrain.FOREST and hColor == Color(0,0,0,0):
			setColor(cell.x, cell.y, Color(1,0,0))
