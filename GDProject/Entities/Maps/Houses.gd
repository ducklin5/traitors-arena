tool
extends MapView

export var computeTiles = true
export(Resource) var noise = PoissonDiscNoise.new()
export var regionRadius = 50
export var count = 2

export var size = Vector2(20,20)
export var roomSize = Vector2(5,5)
export var hallType = 0
export var pad = 0

const GROUND = Color("#CC00FF")

var tiles = {}
func pickPositions(map):
	# initialize the PoissonDiscNoise
	noise._seed = map._seed
	noise.width = int(map.size.x)
	noise.height = int(map.size.y)
	var cells = noise.getSamples()
	
	# get noise cells within the radius
	var validCells = []
	var center:Vector2 = map.size/2
	for cell in cells:
		var distance =  center.distance_to(cell)
		if distance < regionRadius: validCells.append(cell)
	
	# randomly select cells from valid cells
	var selectedCells = []
	for i in min(count, validCells.size()):
		var idx = randi()%validCells.size()
		var cell = validCells[idx]
		validCells.remove(idx)
		selectedCells.append(cell)
	return selectedCells

func _run():
	seed(map._seed)
	tiles = {}
	var FOREST = map.getView("Terrain").FOREST
	# for each house
	for pos in pickPositions(map):
		# set the terrain to SOIL
		var start = (pos - size/2).round() # top left cell of the house 
		var stop = start+size
		for y in range(start.y-pad, stop.y+pad):
			for x in range(start.x-pad, stop.x+pad):
				if computeTiles: setColor(x,y, FOREST)
				else: setColor(x,y, GROUND)
		# build the interior
		if !computeTiles: continue
		var house = House.new(start, size, randi())
		house.roomSize = roomSize
		house.critHallType = hallType
		util.merge_dict(tiles, house.build())
	
	for cell in tiles:
		match tiles[cell]:
			"ground": setColor(cell.x, cell.y, GROUND)
			"wall": setColor(cell.x, cell.y, Color(0,0,0))
	
class House:
	const dirs = [Vector2(0,1), Vector2(0,-1),
			Vector2(1,0), Vector2(-1,0),
			Vector2(1,1), Vector2(1,-1),
			Vector2(-1,1), Vector2(-1,-1)]
	
	var origin:Vector2
	var size:Vector2
	var _seed:int
	var critHallType = 0
	var roomSize:Vector2 = Vector2(5,5)
	
	func _init(_origin:Vector2, _size:Vector2, seedInt:int):
		self.origin = _origin
		self.size = _size
		self._seed = seedInt
	
	func build():
		seed(_seed)
		var house = Rect2(origin,size)
		var rooms = util.bspRect(house, Vector2(6,6), 3)
		var roomStar = AStar.new()
		
		var critRoomIds = getCritRoomIds(house, rooms, roomStar)
		var branches = getBranches(roomStar, critRoomIds)
		
		# get all the critical rooms
		var critRooms = [];
		for id in critRoomIds: critRooms.append(rooms[id])
		
		# get the ground cells
		var groundCells = getInnerCells(critRooms)
		
		# get all the branch rooms
		var branchRooms = [];
		for rootId in branches:
			var root = rooms[rootId]
			var branch = rooms[branches[rootId]]
			branchRooms.append(branch)
			groundCells += getInnerCells([root, branch]) # append the ground tiles
		
		var nonWallCells = groundCells + [ #groundCells
			getRoomWalls(critRooms[0])[3], #frontDoor
			getRoomWalls(critRooms[0])[5], #frontDoor
			getRoomWalls(critRooms[-1])[2], #backDoor
			getRoomWalls(critRooms[-1])[4] ] #backDoor
		
		var walls = []; 
		for room in branchRooms + critRooms: walls += getRoomWalls(room)
		for cell in nonWallCells: walls.erase(cell)
		
		var tiles = {}
		for cell in groundCells: tiles[cell] = "ground"
		for wall in walls: tiles[wall] = "wall"
		return tiles
	
	func getCritRoomIds(house:Rect2, rooms:Array, roomStar:AStar):
		var backRooms = []
		var frontRooms = []
		
		for idxA in rooms.size():
			var roomA = rooms[idxA]
			if roomA.position.y == house.position.y: backRooms.append(idxA)
			elif roomA.end.y == house.end.y: frontRooms.append(idxA)
			
			if !roomStar.has_point(idxA):
				var centerA = roomA.position + roomA.size/2
				centerA = Vector3(centerA.x, centerA.y, 0)
				roomStar.add_point(idxA,centerA)
			
			for idxB in rooms.size():
				if idxA == idxB: continue
				if roomStar.are_points_connected(idxA, idxB): continue
				if rectTouching(roomA, rooms[idxB]):
					if !roomStar.has_point(idxB):
						var centerB = rooms[idxB].position + rooms[idxB].size/2
						centerB = Vector3(centerB.x, centerB.y, 0)
						roomStar.add_point(idxB,centerB)
					roomStar.connect_points(idxA, idxB, true)
		
		var frontIdx = frontRooms[randi()%frontRooms.size()]
		var backIdx = backRooms[randi()%backRooms.size()]
		var critRooms = []
		return Array(roomStar.get_id_path(frontIdx, backIdx))
	
	func rectTouching(rectA:Rect2, rectB:Rect2):
		var shift = (rectA.position - rectB.position).normalized()*sqrt(2)
		rectB.position += shift
		var clip = rectA.clip(rectB)
		rectB.position -= shift
		return clip.get_area() > 4
	
	func getPathCells(rooms:Array, startId:int, endId:int):
		var startPos = rooms[startId].position + rooms[startId].size/2
		var endPos = rooms[endId].position + rooms[endId].size/2
		var cells = getInnerCells(rooms)
		var aStar = AStar.new()
		
		var cellA; var cellB;
		for idxA in cells.size():
			cellA = cells[idxA]
			if !aStar.has_point(idxA):
				aStar.add_point(idxA,Vector3(cellA.x, cellA.y, 0))
			var neighbours = [Vector2.UP + cellA, Vector2.DOWN + cellA,
					Vector2.LEFT + cellA, Vector2.RIGHT + cellA]
			for idxB in cells.size():
				if idxA == idxB: continue
				if aStar.are_points_connected(idxA, idxB): continue
				cellB = cells[idxB]
				if neighbours.has(cellB):
					if !aStar.has_point(idxB):
						aStar.add_point(idxB,Vector3(cellB.x, cellB.y, 0))
					aStar.connect_points(idxA, idxB, true)
		var startCellId = aStar.get_closest_point(Vector3(startPos.x, startPos.y,0))
		var endCellId = aStar.get_closest_point(Vector3(endPos.x, endPos.y, 0))
		var path = []
		for id in aStar.get_id_path(startCellId,endCellId):
			path.append(cells[id])
		return path
	
	func getInnerCells(rooms:Array):
		var cells = []
		for room in rooms: cells += getRoomCells(room)
		var inner = []
		for cell in cells:
			if isSurrounded(cell,cells): inner.append(cell)
		return inner
	
	func isSurrounded(cell,cells):
		for d in dirs: 
			if !cells.has(cell+d): return false
		return true
	
	func getBranches(roomStar:AStar, critRoomIds:Array):
		var branches = {}
		for idx in critRoomIds:
			var roomBranches = Array(roomStar.get_point_connections(idx))
			roomBranches.sort()
			for branch in roomBranches:
				if (!(branch in critRoomIds)
					and !branches.values().has(branch)):
					branches[idx] = branch; break
		return branches
	
	func getRoomCells(room:Rect2):
		var cells = []
		for x in range(room.position.x, room.end.x):
			for y in range(room.position.y, room.end.y):
				cells.append(Vector2(x,y))
		return cells

	func getRoomWalls(room:Rect2):
		var walls = {}
		for x in range(room.position.x, room.end.x):
			walls[Vector2(x, room.position.y)] = 1
			walls[Vector2(x, room.end.y-1)] = 1
		for y in range(room.position.y, room.end.y):
			walls[Vector2(room.position.x, y)] = 1
			walls[Vector2(room.end.x-1, y)] = 1
		return walls.keys()
