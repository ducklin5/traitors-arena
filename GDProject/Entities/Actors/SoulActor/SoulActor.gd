extends "res://Entities/Actors/Actor.gd"
class_name SoulActor
var hitTimer:Timer

func _ready():
	if is_network_master():
		hitTimer = Timer.new()
		hitTimer.connect("timeout", self, "onHitTimeout" )

func posses(prevActor = null):
	if prevActor:
		position = prevActor.position + Vector2(0, 50)
	$CollisionShape.set_disabled(false)
	show()
	if hitTimer:
		hitTimer.start(2)
		add_child(hitTimer)

func onHitTimeout():
	$Health.rpc("damage", null, 4)

func unposses():
	if hitTimer: remove_child(hitTimer)
	$CollisionShape.set_disabled(true)
	hide()