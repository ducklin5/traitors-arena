extends Node
class_name Health

export var health = 0
export var healthMax = 0
export var healthRegen = 0
export var shield = 0
export var shieldMax = 0
export var shieldStrength = 0.6 #Shield damage reduction (fractional)
export var tokens = 10
var lastAttacker:PlayerState = null

signal healthChanged(health, healthMax, shield, shieldMax)
signal dead()

sync func damage(playerPath, amount:float, onShield : bool = true, onHealth : bool = true):
	if !is_network_master() and get_tree().get_rpc_sender_id() != 1: return
	if amount and onShield:
		var shieldDamage = min(shield, amount * (1-shieldStrength))
		shield -= shieldDamage
		if shieldDamage:
			amount -= shieldDamage / (1-shieldStrength)
	
	if amount and onHealth: health -= min(amount, health)
	var player:PlayerState = get_node(playerPath) if playerPath else null
	if player: lastAttacker = player
	emit_signal("healthChanged", health, healthMax, shield, shieldMax)
	if !health:
		emit_signal("dead")
		if lastAttacker: lastAttacker.addKill(tokens) 