extends State
var G = Globals

onready var sprite = getHost().get_node("Layers/Sprite")
export var WALK_SPEED = 128
export var RUN_SPEED = 192
export var run = false

var animNames = {
	G.UP: "walk_up",
	G.DOWN: "walk_down",
	G.LEFT: "walk_side",
	G.RIGHT: "walk_side",
	G.UP + G.LEFT: "walk_up",
	G.UP + G.RIGHT: "walk_up",
	G.DOWN + G.LEFT: "walk_down",
	G.DOWN + G.RIGHT: "walk_down",
}

func process(delta):
	if getHost().type != getHost().Client and !getHost().dirBtns:
		return "idle"
	var facingNib = G.nibFromAngle(getHost().facingAngle)
	sprite.flip_h = (facingNib == G.LEFT)
	sprite.speed_scale = 1 + int(run)
	getHost().currAnim = animNames[facingNib]
	
	# Physics
	var speed = RUN_SPEED if run else WALK_SPEED
	var velocity = G.vectorFromNib(getHost().dirBtns) * speed
	var collision = getHost().move_and_collide(velocity * delta)
	if collision: 
		velocity = velocity.slide(collision.normal)
		getHost().move_and_collide(velocity * delta)
	
		
func exit():
	sprite.speed_scale = 1