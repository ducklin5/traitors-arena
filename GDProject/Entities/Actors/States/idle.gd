extends State
var G = Globals
onready var sprite = getHost().get_node("Layers/Sprite")

var animNames = {
	G.UP: "idle_up",
	G.DOWN: "idle_down",
	G.LEFT: "idle_side",
	G.RIGHT: "idle_side",
	G.UP + G.LEFT: "idle_up",
	G.UP + G.RIGHT: "idle_up",
	G.DOWN + G.LEFT: "idle_down",
	G.DOWN + G.RIGHT: "idle_down",
}

func process(delta):
	# Animation
	var facingNib = G.nibFromAngle(getHost().facingAngle)
	sprite.flip_h = bool(facingNib & G.LEFT)
	getHost().currAnim = animNames[facingNib]
	if getHost().dirBtns: return "walk"