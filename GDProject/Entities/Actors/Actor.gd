""" Base Actor class
+-- {Local}
	Sends inputs to the server
	and Uses inputs in _process and _physics_process
	uses recieved Snapshots to make a prediction
+-- {Server}
	uses recieved inputs in _process and _physics_process
	Sends Snapshots to Local and Client
+-- {Client} 
	interpolated between recieved Snapshots
"""
extends KinematicBody2D
class_name Actor

enum { Server, Local, Client }
const S = StateMachine

# {{ Properties
export var type = Local
var player
var facingAngle = 0
var currAnim = "idle_down"
var transition = S.None
var state:String setget ,getState

var dirBtns = 0 # binary direction state: 0b up down left right
var cursorPos:Vector2 = Vector2(0,0)
var actions:Dictionary = {}

var error:Vector2 = Vector2(0,0)
var newSrvSnap = null
# }}

# {{ Signals
signal playerChanged()
signal doneTick()
signal entityInteraction(entity, entered)
# }}

# ======================
func updateTransition(trans): transition = trans

func onInputReceived(input):
	restoreInput(input)
	doActions(input.actions)
	rpc_unreliable("doActions", actions, false, true)

func onSnapshotReceived(snapshot):
	newSrvSnap = snapshot
# ======================
func tick(time, latency):
	match type:
		Local:
			var input = InputStruct.new()
			input.init(dirBtns, cursorPos, actions, time)
			$Net.saveSendInput(input)
			doActions(actions)
			if newSrvSnap:
				removeOldInputs(newSrvSnap.time)
				reconcile(newSrvSnap, $Net.inputHistory)
		Server:
			$Net.sendSnapshot(getCurrentSnapshot($Net.lastInputTime))
	newSrvSnap = null
	actions = {}
	transition = S.None
	emit_signal("doneTick")

func removeOldInputs(time):
	while $Net.inputHistory.size():
		if $Net.inputHistory.front().time <= time: 
			$Net.inputHistory.pop_front()
		else: break

func reconcile(startSnapshot, inputArray):
	var current = getCurrentSnapshot(0)
	restoreSnapshot(startSnapshot)
	var time
	var delta = get_physics_process_delta_time()
	for i in inputArray.size():
		time = inputArray[i].time
		restoreInput(inputArray[i], true, false)
		doActions(inputArray[i].actions, true)
		if i+1 < inputArray.size():
			while time < inputArray[i+1].time:
				time += delta
				_physics_process(delta)
				incrementTimers(delta)
	var newError = position - current.pos
	if newError.length()>8: error = newError
	position = current.pos; facingAngle = current.rot

func _physics_process(delta):
	match type: 
		Server,Local: facingAngle = cursorPos.angle()
	$States.process(delta)

func incrementTimers(delta):
	for timer in $Timers.get_children():
		var timeLeft = timer.time_left - delta
		if timeLeft > 0 : timer.start(timeLeft)  
		else: timer.stop()

puppet func doActions( actionDict:Dictionary, prediction = false, is_rpc = false):
	if is_rpc and get_tree().get_rpc_sender_id() != 1: return false
	if type == Local and is_rpc: return false
	$States.modifyState("walk", "run", actionDict.has("run"))
	return true
# ======================
func _process(delta):
	if currAnim: $Layers/Sprite.play(currAnim)
	match type:
		Local: errorCorrect(delta)
		Client: interpolate()

func errorCorrect(delta):
	if !$Net.snapshotInterval: return 
	var correction = error*delta*5
	correction = correction.clamped(error.length())
	position += correction
	error -= correction

func interpolate():
	var startSnap = $Net.srvSnaps[1]
	var endSnap = $Net.srvSnaps[0]
	if endSnap and startSnap and $Net.snapshotInterval:
		restoreSnapshot(startSnap)
		var weight = $Net.snapshotTimer/$Net.snapshotInterval
		position = lerp(startSnap.pos, endSnap.pos, weight)
		facingAngle = lerp(startSnap.rot, endSnap.rot, weight)

# ======================
func restoreInput(input :InputStruct, dBtns=true, cPos=true, actns=true):
	if dBtns: dirBtns = input.dirBtns
	if cPos: cursorPos = input.cursorPos
	if actns: actions = input.actions.duplicate()

func getCurrentSnapshot(time = -1) -> Snapshot:
	var timers = {}
	for timer in $Timers.get_children():
		timers[timer.name] = float(timer.time_left)
	var snapshot = Snapshot.new()
	snapshot.init(position, facingAngle, 
		$States.stateStack, transition, $States.getCurrent().clock, 
		timers, time)
	return snapshot

func restoreSnapshot( snap :Snapshot):
	position = snap.pos
	facingAngle = snap.rot
	if snap.transition == S.Exit or snap.transition == S.Transit: 
		$States.getCurrent().exit()
	$States.stateStack = snap.stateStack.duplicate()
	$States.getCurrent().clock = snap.stateDur
	if snap.transition == S.Enter or snap.transition == S.Transit: 
		$States.getCurrent().enter()
	# retsore cooldown timers
	for tname in snap.timers:
		var timer = $Timers.get_node(tname)
		var timeLeft = snap.timers[tname]
		if timeLeft > 0: timer.start(timeLeft)  
		else: timer.stop()
# ======================
puppetsync func enable(playerPath, prevActorPath=null):
	player = get_node(playerPath)
	connect("entityInteraction", player, "updateUsables")
	$Net.peerId = player.peerId
	if get_tree().get_network_unique_id() == player.peerId: type = Local
	reset()
	if prevActorPath: posses(get_node(prevActorPath))
	else: posses()
	$interactArea.monitoring = true
	emit_signal("playerChanged")

puppetsync func disable(resetPeer):
	disconnect("entityInteraction", player, "updateUsables")
	player = null
	if resetPeer: $Net.peerId = 0
	if type != Server: type = Client
	reset()
	unposses()
	$interactArea.monitoring = false
	emit_signal("playerChanged")
	
func reset():
	$Net.srvSnaps = [null,null]
	$Net.inputHistory = []
	dirBtns = 0
	actions = {}

func posses(prevActor = null): pass

func unposses(): pass

func isPossed(): return $Net.peerId > 0

func interaction(body, entered):
	if body == self: return
	emit_signal("entityInteraction", body, entered)

func addLayer(node): $Layers.add_child(node)

func getState():
	return $States.getCurrent().name