extends Node
var peerId = 0

var inputHistory = [] # local
var lastInputTime = -1 # server

# client & local
var srvSnaps = [null,null]
var intervals = []
var snapshotInterval:float = 1
var snapshotTimer:float = 1 #time since last snapshot

signal inputReceived(input)
signal snapshotReceived(snapshot)

# {{ Input and Snapshot (State sync)
func saveSendInput(input: InputStruct):
	inputHistory.push_back(input)
	var dict = inst2dict(input)
	rpc_unreliable_id(1, "recieveInput", dict)

func sendSnapshot(snapshot: Snapshot):
	rpc_unreliable("recieveSnapshot", inst2dict(snapshot))

master func recieveInput(inputDict):
	if ! is_network_master(): return # only the server can run this fuction
	if ( peerId != get_tree().get_rpc_sender_id() ): 
		print ( "False rpc recieved from ", get_tree().get_rpc_sender_id())
		print ( "Only ", peerId, " can RPC this function")
		return # only the peer that owns this player can rpc this
	# discard input packet if it is more that 1 second in the past or future
	# or if it is before the last processed input
	var input = dict2inst(inputDict)
	if input.time < lastInputTime: return
	lastInputTime = input.time
	emit_signal("inputReceived", input)
	
puppet func recieveSnapshot(snapshotDict):
	# security checks
	if ( get_tree().get_rpc_sender_id() != 1 ): return
	var snapshot = dict2inst(snapshotDict)
	
	# dont recieve snapshots untill the server has recieved an input
	if snapshot.time < 0: return
	# if the snapshot is older than the latest processed one, ignore it
	if srvSnaps[0] and snapshot.time <= srvSnaps[0].time: return
	srvSnaps = [snapshot, srvSnaps[0]]
	
	# calculate the average snapshot interval
	intervals.push_front(snapshotTimer)
	if intervals.size() > 15: intervals.pop_back()
	snapshotInterval = 0
	for elem in intervals: snapshotInterval += elem
	snapshotInterval /= intervals.size()
	snapshotTimer = 0
	emit_signal("snapshotReceived", snapshot)

func _process(delta):
	if peerId: snapshotTimer += delta
# }}