extends State

onready var bar:ProgressBar = getHost().get_node("AvailalityBar")
onready var health:Health = getHost().get_node("Health")
export var duration = 60

func enter():
	clock = 0
	bar.show()
	getHost().get_node("Layers/Sprite").modulate = Color(0.5,0,0)

func process(delta):
	getHost().currAnim = "empty"
	clock += delta
	bar.value = clock/duration
	if clock > duration: return "empty"

func exit():
	health.health = health.healthMax
	bar.hide()
	getHost().get_node("Layers/Sprite").modulate = Color(1,1,1)