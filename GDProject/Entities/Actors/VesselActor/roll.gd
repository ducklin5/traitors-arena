extends State

onready var sprite = getHost().get_node("Layers/Sprite")
var direction = Vector2(0,0)
export var speed = 256
export var duration = 0.35

func _init():
	stacks = true

func enter():
	clock = 0
	var tween = getHost().get_node("Tween")
	if !tween.is_active():
		if direction.x < 0: tween.interpolate_property(sprite, "rotation", 2*PI, 0, duration, 0, 0)
		else: tween.interpolate_property(sprite, "rotation", 0, 2*PI, duration, 0, 0)
		tween.start()

func process(delta):
	clock += delta
	if clock > duration: return PREV_STATE
	getHost().move_and_collide(direction*speed*delta)