shader_type canvas_item;

uniform float thickness: hint_range(0.0, 30.0);
uniform vec4 color: hint_color;

void fragment(){
	float uvThick_x = thickness / float(textureSize(TEXTURE,0).x);
	float uvThick_y = thickness / float(textureSize(TEXTURE,0).y);

	vec4 sColor = texture(TEXTURE,UV);
	
	float outlineA = 0.0;
	for (int i = -1; i < 2; i++) {
		for (int j = -1; j < 2; j++) {
			outlineA += texture(TEXTURE, UV + vec2(float(i)*uvThick_x, float(j)*uvThick_y)).a;
		}
	}
	outlineA *= (1.0 - sColor.a);
	
	vec4 finalColor = mix(color, sColor, sColor.a);
	COLOR = vec4(finalColor.rgb, clamp(sColor.a+outlineA, 0,1));
}