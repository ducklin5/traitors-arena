extends State

func enter():
	getHost().get_node("Layers/Sprite").modulate = Color(0.0,0.5,0)
	getHost().onDeath()

func process(delta):
	getHost().currAnim = "empty"
	clock += delta
	if clock > 1 and is_network_master(): rpc("destroy")

sync func destroy():
	if !is_network_master() and get_tree().get_rpc_sender_id() != 1: return
	getHost().queue_free()