extends State
var emptyShader = preload("res://Entities/Actors/VesselActor/EmptyShader.tres")
var prevShader = null
var entered = false
onready var sprite = getHost().get_node("Layers/Sprite")

func enter():
	if entered: return
	clock = 0
	prevShader = sprite.material 
	sprite.material = emptyShader
	entered = true

func process(delta):
	getHost().currAnim = "empty"

func exit():
	if !entered: return
	sprite.material = prevShader
	entered = false