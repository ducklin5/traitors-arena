extends "res://Entities/Actors/Actor.gd"
class_name VesselActor

var team :Team setget setTeam

func setTeam(_team:Team):
	team = _team
	material.set_shader_param('color', _team.color)
	
puppet func doActions( actionDict:Dictionary, prediction = false, is_rpc = false):
	if !.doActions(actionDict, prediction, is_rpc): return false
	for action in actionDict:
		match action:
			"roll":
				if !actionDict[action] or !$Timers/roll.is_stopped(): continue
				else:
					var direction = Globals.vectorFromNib(actionDict[action])
					$States.modifyState("roll", "direction", direction)
					$States.changeState("roll")
					$Timers/roll.start(1)
			"attack":
				continue
				if !prediction:
					$Weapons.current().attack(actionDict[action])
			"next_weapon":
				continue
				if !prediction:
					$Weapons.setIndex(actionDict[action])
					$Weapons.increment(1)
			"prev_weapon":
				continue
				if !prediction:
					$Weapons.setIndex(actionDict[action])
					$Weapons.increment(-1)
	return true

func posses(prevActor = null):
	$States.changeState("idle")

sync func terminate():
	if !is_network_master() and get_tree().get_rpc_sender_id() != 1: return
	$States.changeState("terminate")

func onDeath():
	if player: player.masterPossesSoul()

func unposses():
	if getState() == "terminate": return
	elif !$Health.health:
		$States.changeState("dead")
	else:
		$States.changeState("empty")