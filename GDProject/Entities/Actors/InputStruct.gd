class_name InputStruct
var dirBtns = 0
var cursorPos = Vector2(0,0)
var actions = {}
var time = 0.0
func init(_dirBtns :int, _cursorPos :Vector2, _actions :Dictionary, _time :float):
	self.dirBtns = _dirBtns
	self.cursorPos = _cursorPos
	self.actions = _actions
	self.time = _time