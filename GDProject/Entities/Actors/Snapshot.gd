class_name Snapshot
var pos :Vector2
var rot :float
var stateStack :Array
var transition :int
var stateDur :float
var timers :Dictionary
var time :float
func init(_pos :Vector2 , _rot: float, _stateStack :Array,  _transition :int, _dur: float, _timers: Dictionary, _time: float):
	pos = _pos
	rot = _rot
	stateStack = _stateStack
	transition = _transition
	stateDur = _dur
	timers = _timers
	time = _time