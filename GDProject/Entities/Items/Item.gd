extends RigidBody2D
class_name Item

var data: Dictionary
var id
var displayName
var description
var icon :AtlasTexture = AtlasTexture.new()
var sprite :Sprite = Sprite.new()
var stacks = false
var player

func import(component: GDScript):
	for child in get_children():
		if child is component: return
	add_child(component.new(self))

func _init(itemData):
	# set the variables from the input data
	data = itemData
	id = data["id"]
	displayName = data["name"]
	name = displayName
	description = data["description"]
	if data.has("stacks"):
		stacks = data["stacks"]
	initIcon()
	initSprite()

func initIcon():
	if data.has("icon"):
		icon.atlas = load(data["icon"])
		icon.region = str2var(data["iRect"])
	else:
		icon.atlas = load(data["texture"])
		icon.region = str2var(data["tRect"])

func initSprite():
	sprite.texture = load(data["texture"])
	if data.has("tRect"):
		sprite.region_enabled = true
		sprite.region_rect = str2var(data["tRect"])
	if data.has("tTransform"):
		sprite.transform = str2var(data["tTransform"])
	add_child(sprite)
	
func do(method, args = []):
	if has_method(method):
		return callv(method, args)
		
	for child in get_children():
		if child is Component and child.has_method(method):
			return child.callv(method, args)

func holder() -> Actor:
	if player:
		return player.actor
	return null

func use(type:int):pass