extends Component
class_name CCShape
var cShape : CollisionShape2D =  CollisionShape2D.new()
func _init(item).(item):
	name = "CCShape"
	cShape.shape = CircleShape2D.new()
	cShape.shape.radius = (
		item.data["cRadius"] if item.data.has("cRadius") else 10)
	item.collision_layer = 0
	item.collision_mask = 0 
	item.add_child(cShape)
