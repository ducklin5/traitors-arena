extends Component
class_name CWeapon
var damage setget setDamage,getDamage

func _init(item).(item):
	name = "CWeapon"
	damage = item.data["damage"]
	
func setDamage(value):
	damage = value

func getDamage():
	return damage