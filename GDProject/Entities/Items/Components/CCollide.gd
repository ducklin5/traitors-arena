extends CCShape
class_name CCollide
func _init(item :Item).(item):
	name = "CCollide"
	item.contact_monitor = true
	item.contacts_reported = 10
	item.connect("body_entered", item, "onCollide")