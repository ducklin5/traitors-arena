extends Component
class_name CLifeSpan
var timer: Timer = Timer.new()

func _init(item).(item):
	name = "CLifeSpan"
	timer.one_shot = true
	timer.wait_time = (
		item.data["lifespan"] if item.data.has("lifespan") 
		else 1)
	timer.connect("timeout", item, "onLifeEnd")
	item.add_child(timer)
	timer.start()