extends Component
class_name CEmit
var emitPos :Position2D = Position2D.new()

func _init(item).(item):
	name = "CEmit"
	emitPos.position = str2var(item.data["emitPos"])
	item.add_child(emitPos)

func emit( node :Node2D):
	node.global_position = emitPos.global_position
	item.holder().get_parent().add_child(node)
