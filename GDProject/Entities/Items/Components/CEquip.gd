extends Component
class_name CEquip
func _init(item).(item):
	name = "CEquip"
	item.mode = RigidBody2D.MODE_KINEMATIC
	item.add_to_group("equip")
	
func equip():
	item.position = Vector2(0,-5)
	item.sprite.transform = str2var(item.data["eTransform"])
	_process(0)
	item.holder().addLayer(item)
	
func _process(delta):
	if item.holder():
		item.rotation = item.holder().facingAngle
		item.position.y = -1 * int(item.holder().facingAngle < 0)
		item.sprite.flip_v = (abs(item.holder().facingAngle) > PI/2)

func unequip():
	item.sprite.transform = str2var(item.data["tTransform"])
	if item.holder(): 
		item.get_parent().remove_child(item)