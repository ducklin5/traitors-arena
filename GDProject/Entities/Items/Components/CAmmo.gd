extends Component
class_name CAmmo
var ItemFactory  = load("res://Entities/Items/ItemFactory.gd")
var ammoId:int = 0
var maximum:int = 10
var amount:int = 10

func _init(item).(item):
	name = "CAmmo"
	ammoId = item.data["ammoId"]
	maximum = item.data["ammoMax"]
	amount = maximum

func getAmmoAmount():
	return maximum
	
func setAmmoAmount(val:int):
	amount = val

func addAmmo(val:int):
	var oldAmount = amount
	amount  = clamp(amount+val, 0, maximum)
	return oldAmount - amount

func getAmmoItem()->Item:
	return ItemFactory.createItem(ammoId)