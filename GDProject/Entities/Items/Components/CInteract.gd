extends CCShape
class_name CInteract

func _init(item).(item):
	name = "CInteract"
	item.collision_layer |= 2
	item.add_to_group("interact")
	
func pick(player):
	item.player = player
	cShape.disabled = true
	item.get_parent().remove_child(item)

func drop():
	cShape.disabled = false
	item.position = item.holder().position
	item.holder().get_parent().add_child(item)
	item.player = null