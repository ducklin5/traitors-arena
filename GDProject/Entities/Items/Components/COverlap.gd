extends Component
class_name COverlap

func _init(item:Item).(item):
	name = "COverlap"
	var area = Area2D.new()
	var cShape = CollisionShape2D.new()
	cShape.shape = CircleShape2D.new()
	cShape.shape.radius = (
		item.data["cRadius"] if item.data.has("cRadius") else 10)
	area.add_child(cShape)
	item.add_child(area)
	area.connect("body_entered", item, "onBodyOverlap")