extends Item
class_name MeleeWeapon

var angularSpeed = 4 * PI

func _init(itemData).(itemData):
	import(CWeapon);
	import(CEquip);
	import(CInteract);