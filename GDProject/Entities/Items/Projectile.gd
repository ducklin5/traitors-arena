extends "res://Entities/Items/Item.gd"
class_name Projectile

var damage = 0

func _init(itemData).(itemData):
	import(CSound)
	import(COverlap)
	import(CLifeSpan)
	collision_layer = 1
	collision_mask = 0
	
func onBodyOverlap(body):
	if !is_network_master(): return
	if body is Actor:
		if body.isPossed():
			var bTeam = body.team if "team" in body else null
			if bTeam != player.team:
				var bodyHealth:Health = body.get_node("Health")
				bodyHealth.rpc("damage", player.get_path(), damage)
				rpc("destroy")
	else: rpc("destroy")

func onLifeEnd():
	if !is_network_master(): return
	rpc("destroy")

sync func destroy():
	if is_network_master() or get_tree().get_rpc_sender_id() == 1:
		queue_free()