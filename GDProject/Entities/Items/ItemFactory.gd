extends Node
class_name ItemFactory

static func createItem( itemId :int ) -> Item:
	var data = ItemDatabase.getItemData(itemId)
	var item
	if !data.has("type"):
		print("Item has no type, cannot create!")
	match data["type"]:
		"BaseItem": item = Item.new(data)
		"ProjectileWeapon": item = ProjectileWeapon.new(data)
		"Projectile": item = Projectile.new(data)
		_ : print("Item doesnt have a valid type")
	return item
