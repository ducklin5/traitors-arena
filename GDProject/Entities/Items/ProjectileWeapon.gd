extends "res://Entities/Items/Item.gd"
class_name ProjectileWeapon
var fireCount = 0
var fireTimer = Timer.new()

func _init(itemData).(itemData):
	import(CInteract)
	import(CEquip)
	import(CWeapon)
	import(CSound)
	import(CAmmo)
	import(CEmit)
	add_child(fireTimer)
	fireTimer.connect("timeout", self, "fire")

func use(type:int):
	match type:
		0: trigger()
		1: release()
		2: reload()
	
func trigger():
	if do("getAmmoAmount"): rpc_id(1,"master_trigger")
	else: reload()

master func master_trigger():
	if !validRPC(): return
	var ammo = do("getAmmoAmount")
	if ammo: rpc("client_trigger", ammo, fireCount)

sync func client_trigger(ammo, count):
	fireCount = count
	do("setAmmoAmount", [ammo])
	fire();
	match data["fireType"]:
		"auto": fireTimer.start(1/data["fireRate"]);
		"semi": pass

func fire():
	if !do("getAmmoAmount"): return
	var projectile = do("getAmmoItem")
	projectile.name = name + "_" + String(fireCount)
	if "weapon" in projectile: 
		projectile.weapon = self
	projectile.linear_velocity = Vector2(cos(rotation), sin(rotation)) * data["speed"]
	projectile.player = player
	projectile.damage = do("getDamage")
	do("emit", [projectile])
	do("addAmmo", [-1])
	fireCount += 1

func release(): rpc_id(1,"master_release")

master func master_release():
	if !validRPC(): return
	rpc("client_release")
	
sync func client_release(): fireTimer.stop()

func reload(): pass

func validRPC():
	if !player: return false
	if get_tree().get_rpc_sender_id() != player.peerId: return false
	return true