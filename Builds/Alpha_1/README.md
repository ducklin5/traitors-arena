Alpha 1 Build
----------------

Instructions:
1. Download both the .exe and .pck files and have them in the same folder
2. Run 3 instances of the .exe file (for now the game only start after 2 players have joined teh server)
3. on 1 instance press Host
4. on the other 2 instances press Join
5. The match should automatically start

Gameplay:
1. WASD movemnt or attach a controller and move with the left joy stick
2. hold SHIFT or R1 to sprint
3. press CTRL to roll/dash while moving
4. when near a body a button will pop up to posses it
5. press X to leave a possesed body
6. press B or E to view your bag
